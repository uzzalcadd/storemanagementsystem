﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StoreManagementSystem.Models
{
    public class Item
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage ="Enter Name")]
        [StringLength(100)]
        public string Name { get; set; }

        [Required(ErrorMessage ="Enter Code")]
        [Index(IsUnique = true)]
        [StringLength(20)]
        public string Code { get; set; }

        [StringLength(300)]
        public string Remarks { get; set; }

        [ForeignKey("Color")]
        public int ColorId { get; set; }

        [ForeignKey("UOM")]
        public int UOMId { get; set; }

        public virtual Color Color { get; set; }
        public virtual UOM UOM { get; set; }
    }
}