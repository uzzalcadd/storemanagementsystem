﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace StoreManagementSystem.Models
{
    public class Store
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        [Required(ErrorMessage ="Enter Department Name")]
        [StringLength(100)]
        public string DepartmentName { get; set; }

        [Required(ErrorMessage ="Enter Floor Number")]
        public int Floor { get; set; }

        [Required(ErrorMessage ="Enter Rack name")]
        [StringLength(100)]
        public string Rack { get; set; }

        [Required(ErrorMessage ="Enter cell")]
        [StringLength(20)]
        public string Cell { get; set; }

        [StringLength(100)]
        public string Bin { get; set; }

        public bool? Status { get; set; }


    }
}