﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StoreManagementSystem.Models
{
    public class MKTPerson
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage ="Enter marketing person name")]
        [StringLength(100)]
        public string Name { get; set; }

        [Required(ErrorMessage ="Enter code")]
        [Index(IsUnique = true)]
        [StringLength(20)]
        public string Code { get; set; }

        [Required(ErrorMessage ="Enter Designation")]
        [StringLength(100)]
        public string Designation { get; set; }

    }
}