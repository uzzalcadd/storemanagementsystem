﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StoreManagementSystem.Models
{
    public class DailyStoreShipment
    {
        [Key]
        public int Id { get; set; }

        public string TransactionNo{ get; set; }

        [DataType(DataType.Date),
        DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}",
        ApplyFormatInEditMode = true)]
        public DateTime TransactionDate { get; set; }

        [Required(ErrorMessage ="Enter Style name")]
        [StringLength(50)]
        public string StyleName { get; set; }

        [Required(ErrorMessage ="Enter Order number")]
        public int OrderNo { get; set; }

        [Required(ErrorMessage ="Enter shipped quantity")]
        public double ShippedQty { get; set; }

        [Required(ErrorMessage ="Enter Cartoon Qunatity")]
        public double CartoonQty { get; set; }

        [Required(ErrorMessage ="Enter Ship mode")]
        [StringLength(100)]
        public string Shipmode { get; set; }

        [Required(ErrorMessage ="Enter the vehicle serial number")]
        [StringLength(100)]
        public string VehicleNo { get; set; }

        [Required(ErrorMessage ="Enter Driver Name")]
        [StringLength(50)]
        public string DriverName { get; set; }

        [Required(ErrorMessage ="Enter Delivery Challan number")]
        [StringLength(100)]
        public string DeliveryChallanNo { get; set; }

        [Required(ErrorMessage ="Enter Security lock")]
        [StringLength(50)]
        public string Securitylock { get; set; }

        [Required(ErrorMessage ="Enter Shipment method")]
        [StringLength(20)]
        public string ShippedBy { get; set; }

        [Required(ErrorMessage ="Enter Port number")]
        [StringLength(20)]
        public string PortNo { get; set; }
    }
}