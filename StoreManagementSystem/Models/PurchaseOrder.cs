﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace StoreManagementSystem.Models
{
    public class PurchaseOrder
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage ="Select Requisition")]
        [ForeignKey("Requisition")]
        public int ReqNoId { get; set; }

        public int PurchaseOrderNo { get; set; }

        [DataType(DataType.Date),
        DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}",
        ApplyFormatInEditMode = true)]
        public DateTime PurchaseOrderDate { get; set; }

        [Required(ErrorMessage ="Select Delivery")]
        [ForeignKey("Delivery")]
        public int DeliveryId { get; set; }

        [Required(ErrorMessage ="Select Bill to")]
        [ForeignKey("Billto")]
        public int BillToId { get; set; }

        [Required(ErrorMessage ="Select marketing person")]
        [ForeignKey("MKTPerson")]
        public int MKTPersonId { get; set; }

        [Required(ErrorMessage ="Select currency")]
        [StringLength(20)]
        public string Currency { get; set; }

        [StringLength(10)]
        public string CurrencyRate { get; set; }

        [StringLength(100)]
        public string PaymentTerm { get; set; }

        [Required(ErrorMessage ="Select Shipping method")]
        public string Shipedby { get; set; }

        [StringLength(100)]
        public string TradeTerm { get; set; }

        [StringLength(200)]
        public string Note { get; set; }

        [DefaultValue(true)]
        public bool Status { get; set; }

        public virtual PRM Requisition { get; set; }
        public virtual Delivery Delivery { get; set; }
        public virtual BillTo Billto { get; set; }
        public virtual MKTPerson MKTPerson { get; set; }

    }
}