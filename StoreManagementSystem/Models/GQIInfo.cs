﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace StoreManagementSystem.Models
{
    public class GQIInfo
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("Item")]
        public int ItemId { get; set; }

        public double ReceivedQuantity { get; set; }

        public double QCPassedQuantity { get; set; }

        public double RejectedQuantity { get; set; }

        [ForeignKey("Store")]
        public int StoreId { get; set; }

        [ForeignKey("GoodQualityInspection")]
        public int GQIId { get; set; }

        public virtual GoodQualityInspection GoodQualityInspection { get; set; }
        public virtual Store Store { get; set; }
        public virtual Item Item { get; set; }
    }
}