﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace StoreManagementSystem.Models
{
    public class SMSDbContext : DbContext
    {
        public SMSDbContext():base("SMSDbContext")
        {
            Database.SetInitializer<SMSDbContext>(null);
            Configuration.ProxyCreationEnabled = true;
            Configuration.LazyLoadingEnabled = true;
        }

        public DbSet<BillTo> BillTo { get; set; }
        public DbSet<Color> Color { get; set; }
        public DbSet<UOM> UOM { get; set; }
        public DbSet<DailyStoreShipment> DailyStoreShipment { get; set; }
        public DbSet<Delivery> Delivery { get; set; }
        public DbSet<Department> Department { get; set; }
        public DbSet<GoodQualityInspection> GoodQualityInspection { get; set; }
        public DbSet<GRNmanagement> GRNmanagement { get; set; }
        public DbSet<Item> Item { get; set; }
        public DbSet<MKTPerson> MKTPerson { get; set; }
        public DbSet<PRM> PRM { get; set; }
        public DbSet<PurchaseOrder> PurchaseOrder { get; set; }
        public DbSet<Reqtype> Reqtype { get; set; }
        public DbSet<Store> Store { get; set; }
        public DbSet<Supplier> Supplier { get; set; }
        public DbSet<TransferRequisitionsIssue> TransferRequisitionsIssue { get; set; }
        public DbSet<User> User { get; set; }
        public DbSet<GRNDetails> GRNDetails { get; set; }
        public DbSet<PRMDetails> PRMDetails { get; set; }
        public DbSet<PurchaseOrderDetails> PurchaseOrderDetails { get; set; }
        public DbSet<TransferRequisitionDetails> TransferRequisitionDetails { get; set; }
        public DbSet<GQIInfo> GQIInfo { get; set; }
    }
}