﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace StoreManagementSystem.Models
{
    public class TransferRequisitionsIssue
    {
        [Key]
        public int Id { get; set; }

        public int TransferNo { get; set; }

        [DataType(DataType.Date),
        DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}",
        ApplyFormatInEditMode = true)]
        public DateTime Issuedate { get; set; }

        [DataType(DataType.Date),
        DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}",
        ApplyFormatInEditMode = true)]
        public DateTime? Receivedate { get; set; }

        [Required(ErrorMessage = "Select Requisition type")]
        [StringLength(20)]
        public string RequsitionType { get; set; }

        [Required(ErrorMessage = "Enter Issue buisness unit")]
        [StringLength(100)]
        public string IssueBu { get; set; }

        [Required(ErrorMessage = "Enter Receive buisness unit")]
        [StringLength(100)]
        public string ReceiveBu { get; set; }

        [Required(ErrorMessage = "Enter vehicle number")]
        [StringLength(100)]
        public string VehicleNo { get; set; }

        [ForeignKey("IssueStore")]
        public int IssueStoreId { get; set; }

        [ForeignKey("ReceiveStore")]
        public int ReceiveStoreId { get; set; }

        [Required(ErrorMessage = "Enter driver name")]
        [StringLength(100)]
        public string DriverName { get; set; }

        [Required(ErrorMessage = "Enter challan number")]
        [StringLength(100)]
        public string ChallanNo { get; set; }

        [Required(ErrorMessage = "Enter getpass number")]
        public int GatePassNo { get; set; }

        public string Status { get; set; }

        public virtual Store IssueStore { get; set; }
        public virtual Store ReceiveStore { get; set; }
    }
}