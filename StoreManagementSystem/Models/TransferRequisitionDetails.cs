﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace StoreManagementSystem.Models
{
    public class TransferRequisitionDetails
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("Item")]
        public int ItemId { get; set; }

        public double IssueQuantity { get; set; }

        public int LotNumber { get; set; }

        [ForeignKey("TransferRequisition")]
        public int TransferRequisitionId { get; set; }

        public virtual Item Item { get; set; }
        public virtual TransferRequisitionsIssue TransferRequisition { get; set; }
    }
}