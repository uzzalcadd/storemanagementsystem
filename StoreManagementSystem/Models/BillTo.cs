﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StoreManagementSystem.Models
{
    public class BillTo
    {

        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage ="Enter Name")]
        [StringLength(100)]
        public string Name { get; set; }

        [Required(ErrorMessage ="Enter Code")]
        [Index(IsUnique = true)]
        [StringLength(20)]
        public string Code { get; set; }

        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        public string Address { get; set; }

        [Required(ErrorMessage ="Enter Contact Person name")]
        [StringLength(30)]
        public string ContactPerson { get; set; }

        [DataType(DataType.EmailAddress)]
        public string CPEmail { get; set; }

        public string CPPhone { get; set; }
    }
}