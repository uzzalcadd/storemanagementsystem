﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace StoreManagementSystem.Models
{
    public class Reqtype
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage ="Enter Requisition type name")]
        [StringLength(100)]
        public string ReqName { get; set; }

    }
}