﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace StoreManagementSystem.Models
{
    public class PRMDetails
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("Item")]
        public int ItemId { get; set; }

        public double Quantity { get; set; }

        public string RequiredFor { get; set; }

        public string Remarks { get; set; }

        [ForeignKey("PRM")]
        public int PRMId { get; set; }

        [DefaultValue(true)]
        public bool Status { get; set; }

        public virtual Item Item { get; set; }
        public virtual PRM PRM { get; set; }
    }
}