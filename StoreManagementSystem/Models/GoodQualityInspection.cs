﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StoreManagementSystem.Models
{
    public class GoodQualityInspection
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage ="Enter Sequene")]
        [StringLength(100)]
        public string Sequence { get; set; }

        public DateTime InspectionDate { get; set; }

        [ForeignKey("GRN")]
        public int GRNId { get; set; }

        [ForeignKey("User")]
        public int? CreatedBy { get; set; }

        public virtual GRNmanagement GRN { get; set; }
        public virtual User User { get; set; }
    }
}