﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StoreManagementSystem.Models
{
    public class Color
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public string Remarks { get; set; }
    }
}