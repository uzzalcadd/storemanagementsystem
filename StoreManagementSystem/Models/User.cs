﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace StoreManagementSystem.Models
{
    public class User
    {
        [Key]
        public int Id { get; set; }

        [MaxLength(15)]
        [Required(ErrorMessage = "Code can not be empty")]
        [Index(IsUnique = true)]
        public string Code { get; set; }

        [Required(ErrorMessage = "Name cannot be empty")]
        [MaxLength(100)]
        public string Name { get; set; }

        [Required(ErrorMessage = "Father's Name cannot be empty")]
        [MaxLength(100)]
        public string FathersName { get; set; }

        [Required(ErrorMessage = "Mother's Name cannot be empty")]
        [MaxLength(100)]
        public string MothersName { get; set; }

        [Required(ErrorMessage = "Gender cannot be empty")]
        [MaxLength(100)]
        public string Gender { get; set; }

        [Required(ErrorMessage = "Address cannot be empty")]
        [MaxLength(250)]
        public string Address { get; set; }

        [Required(ErrorMessage = "Mobile cannot be empty")]
        [StringLength(50)]
        public string Mobile { get; set; }

        [RegularExpression(@"^[a-zA-Z]+(([\'\,\.\- ][a-zA-Z ])?[a-zA-Z]*)*\s+<(\w[-._\w]*\w@\w[-._\w]*\w\.\w{2,3})>$|^(\w[-._\w]*\w@\w[-._\w]*\w\.\w{2,3})$", ErrorMessage = "Invalid Email")]
        [StringLength(100)]
        public string Email { get; set; }

        [Required(ErrorMessage = "Password cannot be empty")]
        [MaxLength(100)]
        public string Password { get; set; }

        [Required(ErrorMessage ="Enter User type")]
        [MaxLength(30)]
        public string UserType { get; set; }
    }
}