﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace StoreManagementSystem.Models
{
    public class PRM
    { 

        [Key]
        public int Id { get; set; }

        public int ReqNo { get; set; }

        [Required(ErrorMessage ="Select requisition type")]
        [ForeignKey("ReqType")]
        public int ReqTypeId { get; set; }

        [DataType(DataType.Date),
        DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}",
        ApplyFormatInEditMode = true)]
        public DateTime ReqDate { get; set; }

        [DataType(DataType.Date),
        DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}",
        ApplyFormatInEditMode = true)]
        public DateTime  RequrimentDate { get; set; }

        [Required(ErrorMessage ="Select Department")]
        [ForeignKey("Department")]
        public int DepartmentId { get; set; }

        [StringLength(100)]
        public string Note { get; set; }

        [DefaultValue(true)]
        public bool Status { get; set; }

        public virtual Reqtype ReqType { get; set; }
        public virtual Department Department { get; set; }

    }
}