﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace StoreManagementSystem.Models
{
    public class Department
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage ="Enter Sequene")]
        [StringLength(100)]
        public string Sequence { get; set; }

        [Required(ErrorMessage ="Enter Department Name")]
        [StringLength(100)]
        public string Name { get; set; }
    }
}