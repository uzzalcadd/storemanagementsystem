﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace StoreManagementSystem.Models
{
    public class GRNDetails
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("Item")]
        public int ItemId { get; set; }

        public double OrderedQuantity { get; set; }

        public double ReceivedQuantity { get; set; }

        public double YetToReceive { get; set; }

        public int LotNumber { get; set; }

        [ForeignKey("Supplier")]
        public int SupplierId { get; set; }

        [ForeignKey("GRNmanagement")]
        public int GRNId { get; set; }

        public virtual Item Item { get; set; }
        public virtual GRNmanagement GRNmanagement { get; set; }
        public virtual Supplier Supplier { get; set; }

    }
}