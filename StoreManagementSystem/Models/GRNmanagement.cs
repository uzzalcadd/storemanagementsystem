﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StoreManagementSystem.Models
{
    public class GRNmanagement
    {

        [Key]
        public int Id { get; set; }

        [StringLength(100)]
        public string GRNType { get; set; }

        public int GRNNumber { get; set; }

        [DataType(DataType.Date),
        DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}",
        ApplyFormatInEditMode = true)]
        public DateTime GRNDate { get; set; }

        [DataType(DataType.Date),
        DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}",
        ApplyFormatInEditMode = true)]
        public DateTime GLDate { get; set; }

        [Required(ErrorMessage ="Select Purhcase Order")]
        [ForeignKey("PurchaseOrder")]
        public int PurchaseOrderId { get; set; }

        [StringLength(50)]
        public string Status { get; set; }

        [StringLength(300)]
        public string Remarks { get; set; }

        [DataType(DataType.Date),
        DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}",
        ApplyFormatInEditMode = true)]
        public DateTime ChallanDate { get; set; }

        [Required(ErrorMessage ="Enter Challan Number")]
        public int ChallanNo { get; set; }

        [Required(ErrorMessage ="Enter Gatepass Number")]
        public int GatePassNo { get; set; }

        [Required(ErrorMessage ="Enter Vehicle Number")]
        public string VehicleNo { get; set; }

        public virtual PurchaseOrder PurchaseOrder { get; set; }
        public virtual Supplier Supplier { get; set; }
    }
}