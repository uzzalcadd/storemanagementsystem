namespace StoreManagementSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Change_in_Store : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Stores", "Status", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Stores", "Status");
        }
    }
}
