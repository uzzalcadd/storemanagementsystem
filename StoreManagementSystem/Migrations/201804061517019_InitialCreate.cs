namespace StoreManagementSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BillToes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        Code = c.String(nullable: false, maxLength: 20),
                        Email = c.String(),
                        Address = c.String(),
                        ContactPerson = c.String(nullable: false, maxLength: 30),
                        CPEmail = c.String(),
                        CPPhone = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Code, unique: true);
            
            CreateTable(
                "dbo.Colors",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Remarks = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DailyStoreShipments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TransactionNo = c.String(),
                        TransactionDate = c.DateTime(nullable: false),
                        StyleName = c.String(nullable: false, maxLength: 50),
                        OrderNo = c.Int(nullable: false),
                        ShippedQty = c.Double(nullable: false),
                        CartoonQty = c.Double(nullable: false),
                        Shipmode = c.String(nullable: false, maxLength: 100),
                        VehicleNo = c.String(nullable: false, maxLength: 100),
                        DriverName = c.String(nullable: false, maxLength: 50),
                        DeliveryChallanNo = c.String(nullable: false, maxLength: 100),
                        Securitylock = c.String(nullable: false, maxLength: 50),
                        ShippedBy = c.String(nullable: false, maxLength: 20),
                        PortNo = c.String(nullable: false, maxLength: 20),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Deliveries",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        Code = c.String(nullable: false, maxLength: 20),
                        Email = c.String(),
                        Address = c.String(),
                        ContactPerson = c.String(nullable: false, maxLength: 30),
                        CPEmail = c.String(),
                        CPPhone = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Code, unique: true);
            
            CreateTable(
                "dbo.Departments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Sequence = c.String(nullable: false, maxLength: 100),
                        Name = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.GoodQualityInspections",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Sequence = c.String(nullable: false, maxLength: 100),
                        InspectionDate = c.DateTime(nullable: false),
                        GRNId = c.Int(nullable: false),
                        CreatedBy = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.GRNmanagements", t => t.GRNId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.CreatedBy)
                .Index(t => t.GRNId)
                .Index(t => t.CreatedBy);
            
            CreateTable(
                "dbo.GRNmanagements",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        GRNType = c.String(maxLength: 100),
                        GRNNumber = c.Int(nullable: false),
                        GRNDate = c.DateTime(nullable: false),
                        GLDate = c.DateTime(nullable: false),
                        PurchaseOrderId = c.Int(nullable: false),
                        Status = c.String(maxLength: 50),
                        Remarks = c.String(maxLength: 300),
                        ChallanDate = c.DateTime(nullable: false),
                        ChallanNo = c.Int(nullable: false),
                        GatePassNo = c.Int(nullable: false),
                        VehicleNo = c.String(nullable: false),
                        Supplier_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PurchaseOrders", t => t.PurchaseOrderId, cascadeDelete: true)
                .ForeignKey("dbo.Suppliers", t => t.Supplier_Id)
                .Index(t => t.PurchaseOrderId)
                .Index(t => t.Supplier_Id);
            
            CreateTable(
                "dbo.PurchaseOrders",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ReqNoId = c.Int(nullable: false),
                        PurchaseOrderNo = c.Int(nullable: false),
                        PurchaseOrderDate = c.DateTime(nullable: false),
                        DeliveryId = c.Int(nullable: false),
                        BillToId = c.Int(nullable: false),
                        MKTPersonId = c.Int(nullable: false),
                        Currency = c.String(nullable: false, maxLength: 20),
                        CurrencyRate = c.String(maxLength: 10),
                        PaymentTerm = c.String(maxLength: 100),
                        Shipedby = c.String(nullable: false),
                        TradeTerm = c.String(maxLength: 100),
                        Note = c.String(maxLength: 200),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.BillToes", t => t.BillToId, cascadeDelete: true)
                .ForeignKey("dbo.Deliveries", t => t.DeliveryId, cascadeDelete: true)
                .ForeignKey("dbo.MKTPersons", t => t.MKTPersonId, cascadeDelete: true)
                .ForeignKey("dbo.PRMs", t => t.ReqNoId, cascadeDelete: true)
                .Index(t => t.ReqNoId)
                .Index(t => t.DeliveryId)
                .Index(t => t.BillToId)
                .Index(t => t.MKTPersonId);
            
            CreateTable(
                "dbo.MKTPersons",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        Code = c.String(nullable: false, maxLength: 20),
                        Designation = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Code, unique: true);
            
            CreateTable(
                "dbo.PRMs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ReqNo = c.Int(nullable: false),
                        ReqTypeId = c.Int(nullable: false),
                        ReqDate = c.DateTime(nullable: false),
                        RequrimentDate = c.DateTime(nullable: false),
                        DepartmentId = c.Int(nullable: false),
                        Note = c.String(maxLength: 100),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Departments", t => t.DepartmentId, cascadeDelete: true)
                .ForeignKey("dbo.Reqtypes", t => t.ReqTypeId, cascadeDelete: true)
                .Index(t => t.ReqTypeId)
                .Index(t => t.DepartmentId);
            
            CreateTable(
                "dbo.Reqtypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ReqName = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Suppliers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        Code = c.String(nullable: false, maxLength: 20),
                        Email = c.String(),
                        Address = c.String(),
                        ContactPerson = c.String(nullable: false, maxLength: 30),
                        CPEmail = c.String(),
                        CPPhone = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Code, unique: true);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 15),
                        Name = c.String(nullable: false, maxLength: 100),
                        FathersName = c.String(nullable: false, maxLength: 100),
                        MothersName = c.String(nullable: false, maxLength: 100),
                        Gender = c.String(nullable: false, maxLength: 100),
                        Address = c.String(nullable: false, maxLength: 250),
                        Mobile = c.String(nullable: false, maxLength: 50),
                        Email = c.String(maxLength: 100),
                        Password = c.String(nullable: false, maxLength: 100),
                        UserType = c.String(nullable: false, maxLength: 30),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Code, unique: true);
            
            CreateTable(
                "dbo.GQIInfoes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ItemId = c.Int(nullable: false),
                        ReceivedQuantity = c.Double(nullable: false),
                        QCPassedQuantity = c.Double(nullable: false),
                        RejectedQuantity = c.Double(nullable: false),
                        StoreId = c.Int(nullable: false),
                        GQIId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.GoodQualityInspections", t => t.GQIId, cascadeDelete: true)
                .ForeignKey("dbo.Items", t => t.ItemId, cascadeDelete: true)
                .ForeignKey("dbo.Stores", t => t.StoreId, cascadeDelete: true)
                .Index(t => t.ItemId)
                .Index(t => t.StoreId)
                .Index(t => t.GQIId);
            
            CreateTable(
                "dbo.Items",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        Code = c.String(nullable: false, maxLength: 20),
                        Remarks = c.String(maxLength: 300),
                        ColorId = c.Int(nullable: false),
                        UOMId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Colors", t => t.ColorId, cascadeDelete: true)
                .ForeignKey("dbo.UOMs", t => t.UOMId, cascadeDelete: true)
                .Index(t => t.Code, unique: true)
                .Index(t => t.ColorId)
                .Index(t => t.UOMId);
            
            CreateTable(
                "dbo.UOMs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Stores",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        DepartmentName = c.String(nullable: false, maxLength: 100),
                        Floor = c.Int(nullable: false),
                        Rack = c.String(nullable: false, maxLength: 100),
                        Cell = c.String(nullable: false, maxLength: 20),
                        Bin = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.GRNDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ItemId = c.Int(nullable: false),
                        OrderedQuantity = c.Double(nullable: false),
                        ReceivedQuantity = c.Double(nullable: false),
                        YetToReceive = c.Double(nullable: false),
                        LotNumber = c.Int(nullable: false),
                        SupplierId = c.Int(nullable: false),
                        GRNId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.GRNmanagements", t => t.GRNId, cascadeDelete: true)
                .ForeignKey("dbo.Items", t => t.ItemId, cascadeDelete: true)
                .ForeignKey("dbo.Suppliers", t => t.SupplierId, cascadeDelete: true)
                .Index(t => t.ItemId)
                .Index(t => t.SupplierId)
                .Index(t => t.GRNId);
            
            CreateTable(
                "dbo.PRMDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ItemId = c.Int(nullable: false),
                        Quantity = c.Double(nullable: false),
                        RequiredFor = c.String(),
                        Remarks = c.String(),
                        PRMId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Items", t => t.ItemId, cascadeDelete: true)
                .ForeignKey("dbo.PRMs", t => t.PRMId, cascadeDelete: true)
                .Index(t => t.ItemId)
                .Index(t => t.PRMId);
            
            CreateTable(
                "dbo.PurchaseOrderDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ItemId = c.Int(nullable: false),
                        Quantity = c.Double(nullable: false),
                        Price = c.Double(nullable: false),
                        SuppierId = c.Int(nullable: false),
                        TotalPrice = c.Double(nullable: false),
                        Status = c.Boolean(nullable: false),
                        PurchaseOrderId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Items", t => t.ItemId, cascadeDelete: true)
                .ForeignKey("dbo.PurchaseOrders", t => t.PurchaseOrderId, cascadeDelete: true)
                .ForeignKey("dbo.Suppliers", t => t.SuppierId, cascadeDelete: true)
                .Index(t => t.ItemId)
                .Index(t => t.SuppierId)
                .Index(t => t.PurchaseOrderId);
            
            CreateTable(
                "dbo.TransferRequisitionDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ItemId = c.Int(nullable: false),
                        IssueQuantity = c.Double(nullable: false),
                        LotNumber = c.Int(nullable: false),
                        TransferRequisitionId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Items", t => t.ItemId, cascadeDelete: true)
                .ForeignKey("dbo.TransferRequisitionsIssues", t => t.TransferRequisitionId, cascadeDelete: true)
                .Index(t => t.ItemId)
                .Index(t => t.TransferRequisitionId);
            
            CreateTable(
                "dbo.TransferRequisitionsIssues",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TransferNo = c.Int(nullable: false),
                        Issuedate = c.DateTime(nullable: false),
                        Receivedate = c.DateTime(),
                        RequsitionType = c.String(nullable: false, maxLength: 20),
                        IssueBu = c.String(nullable: false, maxLength: 100),
                        ReceiveBu = c.String(nullable: false, maxLength: 100),
                        VehicleNo = c.String(nullable: false, maxLength: 100),
                        IssueStoreId = c.Int(nullable: false),
                        ReceiveStoreId = c.Int(nullable: false),
                        DriverName = c.String(nullable: false, maxLength: 100),
                        ChallanNo = c.String(nullable: false, maxLength: 100),
                        GatePassNo = c.Int(nullable: false),
                        Status = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Stores", t => t.IssueStoreId, cascadeDelete: true)
                .ForeignKey("dbo.Stores", t => t.ReceiveStoreId, cascadeDelete: false)
                .Index(t => t.IssueStoreId)
                .Index(t => t.ReceiveStoreId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TransferRequisitionDetails", "TransferRequisitionId", "dbo.TransferRequisitionsIssues");
            DropForeignKey("dbo.TransferRequisitionsIssues", "ReceiveStoreId", "dbo.Stores");
            DropForeignKey("dbo.TransferRequisitionsIssues", "IssueStoreId", "dbo.Stores");
            DropForeignKey("dbo.TransferRequisitionDetails", "ItemId", "dbo.Items");
            DropForeignKey("dbo.PurchaseOrderDetails", "SuppierId", "dbo.Suppliers");
            DropForeignKey("dbo.PurchaseOrderDetails", "PurchaseOrderId", "dbo.PurchaseOrders");
            DropForeignKey("dbo.PurchaseOrderDetails", "ItemId", "dbo.Items");
            DropForeignKey("dbo.PRMDetails", "PRMId", "dbo.PRMs");
            DropForeignKey("dbo.PRMDetails", "ItemId", "dbo.Items");
            DropForeignKey("dbo.GRNDetails", "SupplierId", "dbo.Suppliers");
            DropForeignKey("dbo.GRNDetails", "ItemId", "dbo.Items");
            DropForeignKey("dbo.GRNDetails", "GRNId", "dbo.GRNmanagements");
            DropForeignKey("dbo.GQIInfoes", "StoreId", "dbo.Stores");
            DropForeignKey("dbo.GQIInfoes", "ItemId", "dbo.Items");
            DropForeignKey("dbo.Items", "UOMId", "dbo.UOMs");
            DropForeignKey("dbo.Items", "ColorId", "dbo.Colors");
            DropForeignKey("dbo.GQIInfoes", "GQIId", "dbo.GoodQualityInspections");
            DropForeignKey("dbo.GoodQualityInspections", "CreatedBy", "dbo.Users");
            DropForeignKey("dbo.GoodQualityInspections", "GRNId", "dbo.GRNmanagements");
            DropForeignKey("dbo.GRNmanagements", "Supplier_Id", "dbo.Suppliers");
            DropForeignKey("dbo.GRNmanagements", "PurchaseOrderId", "dbo.PurchaseOrders");
            DropForeignKey("dbo.PurchaseOrders", "ReqNoId", "dbo.PRMs");
            DropForeignKey("dbo.PRMs", "ReqTypeId", "dbo.Reqtypes");
            DropForeignKey("dbo.PRMs", "DepartmentId", "dbo.Departments");
            DropForeignKey("dbo.PurchaseOrders", "MKTPersonId", "dbo.MKTPersons");
            DropForeignKey("dbo.PurchaseOrders", "DeliveryId", "dbo.Deliveries");
            DropForeignKey("dbo.PurchaseOrders", "BillToId", "dbo.BillToes");
            DropIndex("dbo.TransferRequisitionsIssues", new[] { "ReceiveStoreId" });
            DropIndex("dbo.TransferRequisitionsIssues", new[] { "IssueStoreId" });
            DropIndex("dbo.TransferRequisitionDetails", new[] { "TransferRequisitionId" });
            DropIndex("dbo.TransferRequisitionDetails", new[] { "ItemId" });
            DropIndex("dbo.PurchaseOrderDetails", new[] { "PurchaseOrderId" });
            DropIndex("dbo.PurchaseOrderDetails", new[] { "SuppierId" });
            DropIndex("dbo.PurchaseOrderDetails", new[] { "ItemId" });
            DropIndex("dbo.PRMDetails", new[] { "PRMId" });
            DropIndex("dbo.PRMDetails", new[] { "ItemId" });
            DropIndex("dbo.GRNDetails", new[] { "GRNId" });
            DropIndex("dbo.GRNDetails", new[] { "SupplierId" });
            DropIndex("dbo.GRNDetails", new[] { "ItemId" });
            DropIndex("dbo.Items", new[] { "UOMId" });
            DropIndex("dbo.Items", new[] { "ColorId" });
            DropIndex("dbo.Items", new[] { "Code" });
            DropIndex("dbo.GQIInfoes", new[] { "GQIId" });
            DropIndex("dbo.GQIInfoes", new[] { "StoreId" });
            DropIndex("dbo.GQIInfoes", new[] { "ItemId" });
            DropIndex("dbo.Users", new[] { "Code" });
            DropIndex("dbo.Suppliers", new[] { "Code" });
            DropIndex("dbo.PRMs", new[] { "DepartmentId" });
            DropIndex("dbo.PRMs", new[] { "ReqTypeId" });
            DropIndex("dbo.MKTPersons", new[] { "Code" });
            DropIndex("dbo.PurchaseOrders", new[] { "MKTPersonId" });
            DropIndex("dbo.PurchaseOrders", new[] { "BillToId" });
            DropIndex("dbo.PurchaseOrders", new[] { "DeliveryId" });
            DropIndex("dbo.PurchaseOrders", new[] { "ReqNoId" });
            DropIndex("dbo.GRNmanagements", new[] { "Supplier_Id" });
            DropIndex("dbo.GRNmanagements", new[] { "PurchaseOrderId" });
            DropIndex("dbo.GoodQualityInspections", new[] { "CreatedBy" });
            DropIndex("dbo.GoodQualityInspections", new[] { "GRNId" });
            DropIndex("dbo.Deliveries", new[] { "Code" });
            DropIndex("dbo.BillToes", new[] { "Code" });
            DropTable("dbo.TransferRequisitionsIssues");
            DropTable("dbo.TransferRequisitionDetails");
            DropTable("dbo.PurchaseOrderDetails");
            DropTable("dbo.PRMDetails");
            DropTable("dbo.GRNDetails");
            DropTable("dbo.Stores");
            DropTable("dbo.UOMs");
            DropTable("dbo.Items");
            DropTable("dbo.GQIInfoes");
            DropTable("dbo.Users");
            DropTable("dbo.Suppliers");
            DropTable("dbo.Reqtypes");
            DropTable("dbo.PRMs");
            DropTable("dbo.MKTPersons");
            DropTable("dbo.PurchaseOrders");
            DropTable("dbo.GRNmanagements");
            DropTable("dbo.GoodQualityInspections");
            DropTable("dbo.Departments");
            DropTable("dbo.Deliveries");
            DropTable("dbo.DailyStoreShipments");
            DropTable("dbo.Colors");
            DropTable("dbo.BillToes");
        }
    }
}
