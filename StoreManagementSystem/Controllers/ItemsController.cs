﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using StoreManagementSystem.Models;

namespace StoreManagementSystem.Controllers
{
    public class ItemsController : Controller
    {
        private SMSDbContext db = new SMSDbContext();

        // GET: Items
        public ActionResult Index()
        {
            var item = db.Item.Include(i => i.Color).Include(i => i.UOM);
            return View(item.ToList());
        }

        #region GetExistingItem
        public ActionResult GetExistingItems()
        {
            string ItemName = Request["ItemName"];
            List<Item> ItemList = new List<Item>();
            if (String.IsNullOrWhiteSpace(ItemName))
            {
                ItemList = db.Item.ToList();
            }
            else
            {
                ItemList = db.Item.Where(i => i.Name.Contains(ItemName)).ToList();
            }
            return Json(ItemList, JsonRequestBehavior.AllowGet);
        }
        #endregion

        // GET: Items/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Item item = db.Item.Find(id);
            if (item == null)
            {
                return HttpNotFound();
            }
            return View(item);
        }

        // GET: Items/Create
        public ActionResult Create()
        {
            ViewBag.ColorId = new SelectList(db.Color, "Id", "Name");
            ViewBag.UOMId = new SelectList(db.UOM, "Id", "Name");
            return View();
        }

        // POST: Items/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Code,Remarks,ColorId,UOMId")] Item item)
        {
            if (ModelState.IsValid)
            {
                db.Item.Add(item);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ColorId = new SelectList(db.Color, "Id", "Name", item.ColorId);
            ViewBag.UOMId = new SelectList(db.UOM, "Id", "Name", item.UOMId);
            return View(item);
        }

        // GET: Items/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Item item = db.Item.Find(id);
            if (item == null)
            {
                return HttpNotFound();
            }
            ViewBag.ColorId = new SelectList(db.Color, "Id", "Name", item.ColorId);
            ViewBag.UOMId = new SelectList(db.UOM, "Id", "Name", item.UOMId);
            return View(item);
        }

        // POST: Items/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Code,Remarks,ColorId,UOMId")] Item item)
        {
            if (ModelState.IsValid)
            {
                db.Entry(item).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ColorId = new SelectList(db.Color, "Id", "Name", item.ColorId);
            ViewBag.UOMId = new SelectList(db.UOM, "Id", "Name", item.UOMId);
            return View(item);
        }

        // GET: Items/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Item item = db.Item.Find(id);
            if (item == null)
            {
                return HttpNotFound();
            }
            return View(item);
        }

        // POST: Items/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Item item = db.Item.Find(id);
            db.Item.Remove(item);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
