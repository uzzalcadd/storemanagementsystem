﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using StoreManagementSystem.Models;

namespace StoreManagementSystem.Controllers
{
    public class ReqtypesController : Controller
    {
        private SMSDbContext db = new SMSDbContext();

        #region List
        // GET: Reqtypes
        public ActionResult Index()
        {
            return View(db.Reqtype.ToList());
        }
        #endregion

        #region Details
        // GET: Reqtypes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Reqtype reqtype = db.Reqtype.Find(id);
            if (reqtype == null)
            {
                return HttpNotFound();
            }
            return View(reqtype);
        }
        #endregion

        #region Create
        // GET: Reqtypes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Reqtypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,ReqName")] Reqtype reqtype)
        {
            if (ModelState.IsValid)
            {
                db.Reqtype.Add(reqtype);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(reqtype);
        }
        #endregion

        #region Edit
        // GET: Reqtypes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Reqtype reqtype = db.Reqtype.Find(id);
            if (reqtype == null)
            {
                return HttpNotFound();
            }
            return View(reqtype);
        }

        // POST: Reqtypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,ReqName")] Reqtype reqtype)
        {
            if (ModelState.IsValid)
            {
                db.Entry(reqtype).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(reqtype);
        }
        #endregion

        #region Delete
        // GET: Reqtypes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Reqtype reqtype = db.Reqtype.Find(id);
            if (reqtype == null)
            {
                return HttpNotFound();
            }
            return View(reqtype);
        }

        // POST: Reqtypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Reqtype reqtype = db.Reqtype.Find(id);
            db.Reqtype.Remove(reqtype);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        #endregion

        #region Dispose

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        #endregion
    }
}
