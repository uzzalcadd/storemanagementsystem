﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using StoreManagementSystem.Models;

namespace StoreManagementSystem.Controllers
{
    public class DailyStoreShipmentsController : Controller
    {
        private SMSDbContext db = new SMSDbContext();

        #region List
        // GET: DailyStoreShipments
        public ActionResult Index()
        {
            return View(db.DailyStoreShipment.ToList());
        }
        #endregion

        #region Details
        // GET: DailyStoreShipments/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DailyStoreShipment dailyStoreShipment = db.DailyStoreShipment.Find(id);
            if (dailyStoreShipment == null)
            {
                return HttpNotFound();
            }
            return View(dailyStoreShipment);
        }
        #endregion

        #region Create
        // GET: DailyStoreShipments/Create
        public ActionResult Create()
        {
            if(db.DailyStoreShipment.Count() > 0)
            {
                ViewBag.TransactionNo = db.DailyStoreShipment.Max(i => i.TransactionNo) + 1;
            }
            else
            {
                ViewBag.TransactionNo = 1;
            }
            return View();
        }

        // POST: DailyStoreShipments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,TransactionNo,TransactionDate,StyleName,OrderNo,ShippedQty,CartoonQty,Shipmode,VehicleNo,DriverName,DeliveryChallanNo,Securitylock,ShippedBy,PortNo")] DailyStoreShipment dailyStoreShipment)
        {
            if (ModelState.IsValid)
            {
                dailyStoreShipment.TransactionDate = DateTime.Now.Date;
                db.DailyStoreShipment.Add(dailyStoreShipment);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(dailyStoreShipment);
        }
        #endregion

        #region Edit
        // GET: DailyStoreShipments/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DailyStoreShipment dailyStoreShipment = db.DailyStoreShipment.Find(id);
            if (dailyStoreShipment == null)
            {
                return HttpNotFound();
            }
            return View(dailyStoreShipment);
        }

        // POST: DailyStoreShipments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,TransactionNo,TransactionDate,StyleName,OrderNo,ShippedQty,CartoonQty,Shipmode,VehicleNo,DriverName,DeliveryChallanNo,Securitylock,ShippedBy,PortNo")] DailyStoreShipment dailyStoreShipment)
        {
            if (ModelState.IsValid)
            {
                db.Entry(dailyStoreShipment).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(dailyStoreShipment);
        }
        #endregion

        #region Delete
        // GET: DailyStoreShipments/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DailyStoreShipment dailyStoreShipment = db.DailyStoreShipment.Find(id);
            if (dailyStoreShipment == null)
            {
                return HttpNotFound();
            }
            return View(dailyStoreShipment);
        }

        // POST: DailyStoreShipments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DailyStoreShipment dailyStoreShipment = db.DailyStoreShipment.Find(id);
            db.DailyStoreShipment.Remove(dailyStoreShipment);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        #endregion

        #region Print
        public ActionResult DailyStoreShipmentByDateRange()
        {
            List<DailyStoreShipment> ShipmentList = new List<DailyStoreShipment>();
            return View(ShipmentList);
        }
        public ActionResult DailyStoreShipmentByDateRangeView()
        {
            DateTime FromDate = Convert.ToDateTime(Request["FromDate"]);
            DateTime ToDate = Convert.ToDateTime(Request["ToDate"]);
            List<DailyStoreShipment> ShipmentList = db.DailyStoreShipment.Where(t => t.TransactionDate >= FromDate && t.TransactionDate <= ToDate).ToList();
            Session["ShipmentList"] = ShipmentList;
            return View("DailyStoreShipmentByDateRange", ShipmentList);
        }

        public ActionResult PrintDailyStoreShipmentByDateRange()
        {
            List<DailyStoreShipment> ShipmentList = new List<DailyStoreShipment>();
            ShipmentList = (List<DailyStoreShipment>)Session["ShipmentList"];
            Session["ShipmentList"] = null;
            return View(ShipmentList);
        }
        #endregion

        #region Dipose
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        #endregion
    }
}
