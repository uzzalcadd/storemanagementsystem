﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using StoreManagementSystem.Models;

namespace StoreManagementSystem.Controllers
{
    public class TransferRequisitionsIssuesController : Controller
    {
        private SMSDbContext db = new SMSDbContext();

        #region List
        // GET: TransferRequisitionsIssues
        public ActionResult Index()
        {
            ViewBag.Status = "Issued";
            var transferRequisitionsIssue = db.TransferRequisitionsIssue.Include(t => t.IssueStore).Include(t => t.ReceiveStore).Where(t => t.Status == "Issued");
            return View(transferRequisitionsIssue.ToList());
        }
        public ActionResult ReceivedIndex()
        {
            ViewBag.Status = "Received";
            var transferRequisitionsIssue = db.TransferRequisitionsIssue.Include(t => t.IssueStore).Include(t => t.ReceiveStore).Where(t => t.Status == "Received");
            return View(transferRequisitionsIssue.ToList());
        }
        #endregion

        #region Details

        // GET: TransferRequisitionsIssues/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TransferRequisitionsIssue transferRequisitionsIssue = db.TransferRequisitionsIssue.Find(id);
            if (transferRequisitionsIssue == null)
            {
                return HttpNotFound();
            }
            return View(transferRequisitionsIssue);
        }
        #endregion

        #region GetExistingStore
        public ActionResult GetExistingStores()
        {
            string StoreName = Request["StoreName"];
            List<Store> StoreList = new List<Store>();
            if (String.IsNullOrWhiteSpace(StoreName))
            {
                StoreList = db.Store.Where(i => i.Status == false).ToList();
            }
            else
            {
                StoreList = db.Store.Where(i => i.Name.Contains(StoreName)).ToList();
            }
            return Json(StoreList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetExistingReceiveStores()
        {
            string ReceiveStoreName = Request["RecieveStoreName"];
            List<Store> StoreList = new List<Store>();
            if (String.IsNullOrWhiteSpace(ReceiveStoreName))
            {
                StoreList = db.Store.Where(i => i.Status == true).ToList();
            }
            else
            {
                StoreList = db.Store.Where(i => i.Name.Contains(ReceiveStoreName)).ToList();
            }
            return Json(StoreList, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region GetExistingItems

        public ActionResult GetExistingItems()
        {
            string ItemName = Request["ItemName"];
            int IssueStoreId = Convert.ToInt32(Request["StoreId"]);
            List<GQIInfo> ItemList = new List<GQIInfo>();
            if (String.IsNullOrWhiteSpace(ItemName))
            {
                ItemList = db.GQIInfo.Where(i => i.StoreId == IssueStoreId).ToList();
            }
            else
            {
                ItemList = db.GQIInfo.Where(i => i.StoreId == IssueStoreId).Where(i => i.Item.Name.Contains(ItemName)).ToList();
            }
            return Json(ItemList, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region GetSelectedItems
        [AllowAnonymous]
        [HttpPost]
        public ActionResult GetSelectedItems()
        {
            if (Request["ItemId"].ToString() != "")
            {
                int ItemId = Convert.ToInt32(Request["ItemId"]);
                double IssueQuantity = Convert.ToDouble(Request["IssueQuantity"]);
                int LotNumber = Convert.ToInt32(Request["LotNumber"]);

                List<TransferRequisitionDetails> TRList = new List<TransferRequisitionDetails>();
                TRList = (List<TransferRequisitionDetails>)Session["TRList"];
                if (TRList.Count() > 0)
                {
                    TransferRequisitionDetails details = new TransferRequisitionDetails();
                    details.Id = TRList.Count() + 1;
                    details.ItemId = ItemId;
                    details.IssueQuantity = IssueQuantity;
                    details.LotNumber = LotNumber;
                    details.Item = db.Item.Find(ItemId);
                    TRList.Add(details);
                    Session["TRList"] = TRList;
                }
                else
                {
                    List<TransferRequisitionDetails> TRLst = new List<TransferRequisitionDetails>();
                    TransferRequisitionDetails details = new TransferRequisitionDetails();
                    details.Id = 1;
                    details.ItemId = ItemId;
                    details.IssueQuantity = IssueQuantity;
                    details.LotNumber = LotNumber;
                    details.Item = db.Item.Find(ItemId);
                    TRLst.Add(details);
                    Session["TRList"] = TRLst;
                }
            }
            return Json(Session["TRList"], JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult DeleteFromList()
        {
            int DetailsId = Convert.ToInt32(Request["DetailsId"]);
            List<TransferRequisitionDetails> TRLst = new List<TransferRequisitionDetails>();
            TRLst = (List<TransferRequisitionDetails>)Session["TRList"];
            for (int i = 0; i < TRLst.Count; i++)
            {
                if (TRLst[i].Id == DetailsId)
                {
                    TRLst.Remove(TRLst[i]);
                }
            }
            Session["TRList"] = null;

            if (TRLst.Count > 0)
            {
                Session["TRList"] = TRLst;
            }
            else
            {
                Session["TRList"] = new List<TransferRequisitionDetails>();
            }

            return Json(Session["TRList"], JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Create

        // GET: TransferRequisitionsIssues/Create
        public ActionResult Create()
        {
            if (db.TransferRequisitionsIssue.ToList().Count > 0)
            {
                ViewBag.TransferNo = db.TransferRequisitionsIssue.Max(i => i.TransferNo) + 1;
            }
            else
            {
                ViewBag.TransferNo = 1;
            }
            Session["TRList"] = new List<TransferRequisitionDetails>();
            return View();
        }

        // POST: TransferRequisitionsIssues/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,TransferNo,Issuedate,Receivedate,RequsitionType,IssueBu,ReceiveBu,VehicleNo,IssueStoreId,ReceiveStoreId,DriverName,ChallanNo,GatePassNo,Status")] TransferRequisitionsIssue transferRequisitionsIssue)
        {
            List<TransferRequisitionDetails> TRLst = new List<TransferRequisitionDetails>();
            TRLst = (List<TransferRequisitionDetails>)Session["TRList"];
            if (TRLst.Count > 0)
            {
                transferRequisitionsIssue.Issuedate = DateTime.Now.Date;
                transferRequisitionsIssue.Status = "Issued";
                db.TransferRequisitionsIssue.Add(transferRequisitionsIssue);
                db.SaveChanges();

                #region Add Details
                List<TransferRequisitionDetails> TRList = new List<TransferRequisitionDetails>();
                TRList = (List<TransferRequisitionDetails>)Session["TRList"];

                for (int i = 0; i < TRList.Count; i++)
                {
                    TransferRequisitionDetails details = new TransferRequisitionDetails();
                    details.ItemId = TRList[i].ItemId;
                    details.IssueQuantity = TRList[i].IssueQuantity;
                    details.LotNumber = TRList[i].LotNumber;
                    details.TransferRequisitionId = transferRequisitionsIssue.Id;
                    db.TransferRequisitionDetails.Add(details);
                    db.SaveChanges();
                }
                #endregion

                return RedirectToAction("Index");
            }
            ViewBag.TransferNo = transferRequisitionsIssue.TransferNo;
            return View(transferRequisitionsIssue);
        }
        #endregion

        #region Edit

        // GET: TransferRequisitionsIssues/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TransferRequisitionsIssue transferRequisitionsIssue = db.TransferRequisitionsIssue.Find(id);
            if (transferRequisitionsIssue == null)
            {
                return HttpNotFound();
            }
            ViewBag.IssueStoreId = new SelectList(db.Store, "Id", "Name", transferRequisitionsIssue.IssueStoreId);
            ViewBag.ReceiveStoreId = new SelectList(db.Store, "Id", "Name", transferRequisitionsIssue.ReceiveStoreId);
            return View(transferRequisitionsIssue);
        }

        // POST: TransferRequisitionsIssues/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,TransferNo,Issuedate,Receivedate,RequsitionType,IssueBu,ReceiveBu,VehicleNo,IssueStoreId,ReceiveStoreId,DriverName,ChallanNo,GatePassNo,Status")] TransferRequisitionsIssue transferRequisitionsIssue)
        {
            if (ModelState.IsValid)
            {
                db.Entry(transferRequisitionsIssue).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IssueStoreId = new SelectList(db.Store, "Id", "Name", transferRequisitionsIssue.IssueStoreId);
            ViewBag.ReceiveStoreId = new SelectList(db.Store, "Id", "Name", transferRequisitionsIssue.ReceiveStoreId);
            return View(transferRequisitionsIssue);
        }

        #endregion

        #region Delete
        // GET: TransferRequisitionsIssues/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TransferRequisitionsIssue transferRequisitionsIssue = db.TransferRequisitionsIssue.Find(id);
            if (transferRequisitionsIssue == null)
            {
                return HttpNotFound();
            }
            return View(transferRequisitionsIssue);
        }

        // POST: TransferRequisitionsIssues/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TransferRequisitionsIssue transferRequisitionsIssue = db.TransferRequisitionsIssue.Find(id);
            db.TransferRequisitionsIssue.Remove(transferRequisitionsIssue);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        #endregion

        #region Receive
        // GET: TransferRequisitionIssues/Delete/5
        public ActionResult Receive(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TransferRequisitionsIssue transferRequisitionIssue = db.TransferRequisitionsIssue.Find(id);
            if (transferRequisitionIssue == null)
            {
                return HttpNotFound();
            }
            return View(transferRequisitionIssue);
        }

        // POST: TransferRequisitionIssues/Delete/5
        [HttpPost, ActionName("Receive")]
        [ValidateAntiForgeryToken]
        public ActionResult ReceiveConfirmed(int id)
        {
            TransferRequisitionsIssue transferRequisitionIssue = db.TransferRequisitionsIssue.Find(id);
            transferRequisitionIssue.Status = "Received";
            transferRequisitionIssue.Receivedate = DateTime.Now;
            db.Entry(transferRequisitionIssue).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("ReceivedIndex");
        }
        #endregion

        #region Print
        public ActionResult TransferRequisitionByDateRange()
        {
            List<TransferRequisitionsIssue> TransferRequisitionList = new List<TransferRequisitionsIssue>();
            return View(TransferRequisitionList);
        }
        public ActionResult TransferRequisitionByDateRangeView()
        {
            DateTime FromDate = Convert.ToDateTime(Request["FromDate"]);
            DateTime ToDate = Convert.ToDateTime(Request["ToDate"]);
            List<TransferRequisitionsIssue> TransferRequisitionList = db.TransferRequisitionsIssue.Where(t => (t.Issuedate >= FromDate && t.Issuedate <= ToDate) || (t.Receivedate >= FromDate && t.Receivedate <= ToDate)).ToList();
            List<TransferRequisitionDetails> TransferRequisitionDetailsList = new List<TransferRequisitionDetails>();
            if (FromDate != null && TransferRequisitionList.Count() > 0)
            {
                foreach (var item in TransferRequisitionList)
                {
                    List<TransferRequisitionDetails> detailsList = db.TransferRequisitionDetails.Where(i => i.TransferRequisitionId == item.Id).ToList();
                    foreach (var itm in detailsList)
                    {
                        TransferRequisitionDetails details = db.TransferRequisitionDetails.Find(itm.Id);
                        TransferRequisitionDetailsList.Add(details);
                    }
                }
            }
            Session["TransferRequisitionList"] = TransferRequisitionList;
            Session["TransferRequisitionDetailsList"] = TransferRequisitionDetailsList;
            return View("TransferRequisitionByDateRange", TransferRequisitionList);
        }

        public ActionResult PrintTransferRequisitionByDateRange()
        {
            List<TransferRequisitionsIssue> TransferRequisitionList = new List<TransferRequisitionsIssue>();
            TransferRequisitionList = (List<TransferRequisitionsIssue>)Session["TransferRequisitionList"];
            Session["TransferRequisitionList"] = null;
            return View(TransferRequisitionList);
        }
        #endregion

        #region Dispose

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        #endregion
    }
}
