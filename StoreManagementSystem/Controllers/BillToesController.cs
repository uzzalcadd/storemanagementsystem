﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using StoreManagementSystem.Models;

namespace StoreManagementSystem.Controllers
{
    public class BillToesController : Controller
    {
        private SMSDbContext db = new SMSDbContext();

        // GET: BillToes
        public ActionResult Index()
        {
            return View(db.BillTo.ToList());
        }

        // GET: BillToes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BillTo billTo = db.BillTo.Find(id);
            if (billTo == null)
            {
                return HttpNotFound();
            }
            return View(billTo);
        }

        #region GetExistingBillto Information
        public ActionResult GetExistingBillToes()
        {
            string SupplierName = Request["SupplierName"];
            List<Supplier> SupplierList = new List<Supplier>();
            if (String.IsNullOrWhiteSpace(SupplierName))
            {
                SupplierList = db.Supplier.ToList();
            }
            else
            {
                SupplierList = db.Supplier.Where(i => i.Name.Contains(SupplierName)).ToList();
            }
            return Json(SupplierList, JsonRequestBehavior.AllowGet);
        }
        #endregion

        // GET: BillToes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: BillToes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Code,Email,Address,ContactPerson,CPEmail,CPPhone")] BillTo billTo)
        {
            if (ModelState.IsValid)
            {
                db.BillTo.Add(billTo);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(billTo);
        }

        // GET: BillToes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BillTo billTo = db.BillTo.Find(id);
            if (billTo == null)
            {
                return HttpNotFound();
            }
            return View(billTo);
        }

        // POST: BillToes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Code,Email,Address,ContactPerson,CPEmail,CPPhone")] BillTo billTo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(billTo).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(billTo);
        }

        // GET: BillToes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BillTo billTo = db.BillTo.Find(id);
            if (billTo == null)
            {
                return HttpNotFound();
            }
            return View(billTo);
        }

        // POST: BillToes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BillTo billTo = db.BillTo.Find(id);
            db.BillTo.Remove(billTo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
