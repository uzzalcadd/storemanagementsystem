﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using StoreManagementSystem.Models;

namespace StoreManagementSystem.Controllers
{
    public class GoodQualityInspectionsController : Controller
    {
        private SMSDbContext db = new SMSDbContext();

        // GET: GoodQualityInspections
        public ActionResult Index()
        {
            var goodQualityInspection = db.GoodQualityInspection.Include(g => g.GRN).Include(g => g.User);
            return View(goodQualityInspection.ToList());
        }

        // GET: GoodQualityInspections/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GoodQualityInspection goodQualityInspection = db.GoodQualityInspection.Find(id);
            if (goodQualityInspection == null)
            {
                return HttpNotFound();
            }
            return View(goodQualityInspection);
        }

        #region GetExistingItem
        public ActionResult GetExistingItems()
        {
            string ItemName = Request["ItemName"];
            int GRNId = Convert.ToInt32(Request["GRNId"]);
            List<GRNDetails> ItemList = new List<GRNDetails>();
            if (String.IsNullOrWhiteSpace(ItemName))
            {
                ItemList = db.GRNDetails.Where(i => i.GRNId == GRNId).ToList();
            }
            else
            {
                ItemList = db.GRNDetails.Where(i => i.GRNId == GRNId && i.Item.Name.Contains(ItemName)).ToList();
            }
            return Json(ItemList, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region GetExistingStore
        public ActionResult GetExistingStores()
        {
            string StoreName = Request["StoreName"];
            List<Store> ItemList = new List<Store>();
            if (String.IsNullOrWhiteSpace(StoreName))
            {
                ItemList = db.Store.Where(i => i.Status == true).ToList();
            }
            else
            {
                ItemList = db.Store.Where(i => i.Status == true && i.Name.Contains(StoreName)).ToList();
            }
            return Json(ItemList, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region GetSelectedItems
        [AllowAnonymous]
        [HttpPost]
        public ActionResult GetSelectedItems()
        {
            if (Request["ItemId"].ToString() != "")
            {
                int ItemId = Convert.ToInt32(Request["ItemId"]);
                double ReceivedQuantity = Convert.ToDouble(Request["ReceivedQuantity"]);
                double QCPassedQuantity = Convert.ToDouble(Request["QCPassedQuantity"]);
                double RejectedQuantity = Convert.ToDouble(Request["RejectedQuantity"]);
                int StoreId = Convert.ToInt32(Request["StoreId"]);

                List<GQIInfo> ItemList = new List<GQIInfo>();
                ItemList = (List<GQIInfo>)Session["ItemList"];
                if (ItemList.Count() > 0)
                {
                    GQIInfo details = new GQIInfo();
                    details.Id = ItemList.Count() + 1;
                    details.ItemId = ItemId;
                    details.ReceivedQuantity = ReceivedQuantity;
                    details.QCPassedQuantity = QCPassedQuantity;
                    details.RejectedQuantity = RejectedQuantity;
                    details.StoreId = StoreId;
                    details.Item = db.Item.Find(ItemId);
                    details.Store = db.Store.Find(StoreId);
                    ItemList.Add(details);
                    Session["ItemList"] = ItemList;
                }
                else
                {
                    List<GQIInfo> ItemLst = new List<GQIInfo>();
                    GQIInfo details = new GQIInfo();
                    details.Id = 1;
                    details.Id = ItemList.Count() + 1;
                    details.ItemId = ItemId;
                    details.ReceivedQuantity = ReceivedQuantity;
                    details.QCPassedQuantity = QCPassedQuantity;
                    details.RejectedQuantity = RejectedQuantity;
                    details.StoreId = StoreId;
                    details.Item = db.Item.Find(ItemId);
                    details.Store = db.Store.Find(StoreId);
                    ItemLst.Add(details);
                    Session["ItemList"] = ItemLst;
                }
            }
            return Json(Session["ItemList"], JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult DeleteFromList()
        {
            int DetailsId = Convert.ToInt32(Request["DetailsId"]);
            List<GQIInfo> ItemLst = new List<GQIInfo>();
            ItemLst = (List<GQIInfo>)Session["ItemList"];
            for (int i = 0; i < ItemLst.Count; i++)
            {
                if (ItemLst[i].Id == DetailsId)
                {
                    ItemLst.Remove(ItemLst[i]);
                }
            }
            Session["ItemList"] = null;

            if (ItemLst.Count > 0)
            {
                Session["ItemList"] = ItemLst;
            }
            else
            {
                Session["ItemList"] = new List<GQIInfo>();
            }

            return Json(Session["ItemList"], JsonRequestBehavior.AllowGet);
        }
        #endregion

        // GET: GoodQualityInspections/Create
        public ActionResult Create()
        {
            ViewBag.GRNId = new SelectList(db.GRNmanagement, "Id", "GRNNumber");
            ViewBag.CreatedBy = new SelectList(db.User, "Id", "Name");
            Session["ItemList"] = new List<GQIInfo>();
            return View();
        }

        // POST: GoodQualityInspections/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Sequence,GRNId,CreatedBy")] GoodQualityInspection goodQualityInspection)
        {
            List<GQIInfo> ItemLst = new List<GQIInfo>();
            ItemLst = (List<GQIInfo>)Session["ItemList"];
            if (ItemLst.Count >0)
            {
                goodQualityInspection.InspectionDate = DateTime.Now.Date;
                goodQualityInspection.CreatedBy = Convert.ToInt32(Session["ADMINID"]);
                db.GoodQualityInspection.Add(goodQualityInspection);
                db.SaveChanges();

                #region Add Details
                List<GQIInfo> ItemList = new List<GQIInfo>();
                ItemList = (List<GQIInfo>)Session["ItemList"];

                for (int i = 0; i < ItemList.Count; i++)
                {
                    GQIInfo details = new GQIInfo();
                    details.ItemId = ItemList[i].ItemId;
                    details.ReceivedQuantity = ItemList[i].ReceivedQuantity;
                    details.QCPassedQuantity = ItemList[i].QCPassedQuantity;
                    details.RejectedQuantity = ItemList[i].RejectedQuantity;
                    details.StoreId = ItemList[i].StoreId;
                    Store store = db.Store.Find(ItemList[i].StoreId);
                    store.Status = false;
                    details.GQIId = goodQualityInspection.Id;
                    db.GQIInfo.Add(details);
                    db.SaveChanges();
                }
                //for (int i = 0; i < ItemList.Count; i++)
                //{
                //    Store store = db.Store.Where(a => a.Id == ItemList[i].StoreId).FirstOrDefault();
                //    store.Status = false;
                // }
                #endregion

                return RedirectToAction("Index");
            }

            ViewBag.GRNId = new SelectList(db.GRNmanagement, "Id", "GRNNumber", goodQualityInspection.GRNId);
            ViewBag.CreatedBy = new SelectList(db.User, "Id", "Name", goodQualityInspection.CreatedBy);
            return View(goodQualityInspection);
        }

        // GET: GoodQualityInspections/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GoodQualityInspection goodQualityInspection = db.GoodQualityInspection.Find(id);
            if (goodQualityInspection == null)
            {
                return HttpNotFound();
            }
            ViewBag.GRNId = new SelectList(db.GRNmanagement, "Id", "GRNNumber", goodQualityInspection.GRNId);
            ViewBag.CreatedBy = new SelectList(db.User, "Id", "Code", goodQualityInspection.CreatedBy);
            return View(goodQualityInspection);
        }

        // POST: GoodQualityInspections/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Sequence,GRNId,CreatedBy")] GoodQualityInspection goodQualityInspection)
        {
            if (ModelState.IsValid)
            {
                db.Entry(goodQualityInspection).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.GRNId = new SelectList(db.GRNmanagement, "Id", "GRNNumber", goodQualityInspection.GRNId);
            ViewBag.CreatedBy = new SelectList(db.User, "Id", "Code", goodQualityInspection.CreatedBy);
            return View(goodQualityInspection);
        }

        // GET: GoodQualityInspections/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GoodQualityInspection goodQualityInspection = db.GoodQualityInspection.Find(id);
            if (goodQualityInspection == null)
            {
                return HttpNotFound();
            }
            return View(goodQualityInspection);
        }

        // POST: GoodQualityInspections/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            GoodQualityInspection goodQualityInspection = db.GoodQualityInspection.Find(id);
            db.GoodQualityInspection.Remove(goodQualityInspection);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        #region Print
        public ActionResult GoodQualityInspectionByDateRange()
        {
            List<GoodQualityInspection> GoodQualityInspectionList = new List<GoodQualityInspection>();
            return View(GoodQualityInspectionList);
        }
        public ActionResult GoodQualityInspectionByDateRangeView()
        {
            DateTime FromDate = Convert.ToDateTime(Request["FromDate"]);
            DateTime ToDate = Convert.ToDateTime(Request["ToDate"]);
            List<GoodQualityInspection> GoodQualityInspectionList = db.GoodQualityInspection.Where(t => (t.InspectionDate >= FromDate && t.InspectionDate <= ToDate)).ToList();
            List<GQIInfo> GQIDetailsList = new List<GQIInfo>();
            if (FromDate != null && GoodQualityInspectionList.Count() > 0)
            {
                foreach (var item in GoodQualityInspectionList)
                {
                    List<GQIInfo> detailsList = db.GQIInfo.Where(i => i.GQIId == item.Id).ToList();
                    foreach (var itm in detailsList)
                    {
                        GQIInfo details = db.GQIInfo.Find(itm.Id);
                        GQIDetailsList.Add(details);
                    }
                }
            }
            Session["GoodQualityInspectionnList"] = GoodQualityInspectionList;
            Session["GQIDetailsList"] = GQIDetailsList;
            return View("GoodQualityInspectionByDateRange", GoodQualityInspectionList);
        }

        public ActionResult PrintGoodQualityInspectionByDateRangeByDateRange()
        {
            List<GoodQualityInspection> GoodQualityInspectionList = new List<GoodQualityInspection>();
            GoodQualityInspectionList = (List<GoodQualityInspection>)Session["GoodQualityInspectionnList"];
            Session["GoodQualityInspectionnList"] = null;
            return View(GoodQualityInspectionList);
        }
        #endregion

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
