﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using StoreManagementSystem.Models;

namespace StoreManagementSystem.Controllers
{
    public class GQIInfoesController : Controller
    {
        private SMSDbContext db = new SMSDbContext();

        // GET: GQIInfoes
        public ActionResult Index()
        {
            var gQIInfo = db.GQIInfo.Include(g => g.GoodQualityInspection).Include(g => g.Item).Include(g => g.Store);
            return View(gQIInfo.ToList());
        }

        // GET: GQIInfoes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GQIInfo gQIInfo = db.GQIInfo.Find(id);
            if (gQIInfo == null)
            {
                return HttpNotFound();
            }
            return View(gQIInfo);
        }

        // GET: GQIInfoes/Create
        public ActionResult Create()
        {
            ViewBag.GQIId = new SelectList(db.GoodQualityInspection, "Id", "Id");
            ViewBag.ItemId = new SelectList(db.Item, "Id", "Name");
            ViewBag.StoreId = new SelectList(db.Store, "Id", "Name");
            return View();
        }

        // POST: GQIInfoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,ItemId,ReceivedQuantity,QCPassedQuantity,RejectedQuantity,FloorNo,RackNo,Cell,StoreId,GQIId")] GQIInfo gQIInfo)
        {
            if (ModelState.IsValid)
            {
                db.GQIInfo.Add(gQIInfo);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.GQIId = new SelectList(db.GoodQualityInspection, "Id", "Id", gQIInfo.GQIId);
            ViewBag.ItemId = new SelectList(db.Item, "Id", "Name", gQIInfo.ItemId);
            ViewBag.StoreId = new SelectList(db.Store, "Id", "Name", gQIInfo.StoreId);
            return View(gQIInfo);
        }

        // GET: GQIInfoes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GQIInfo gQIInfo = db.GQIInfo.Find(id);
            if (gQIInfo == null)
            {
                return HttpNotFound();
            }
            ViewBag.GQIId = new SelectList(db.GoodQualityInspection, "Id", "Id", gQIInfo.GQIId);
            ViewBag.ItemId = new SelectList(db.Item, "Id", "Name", gQIInfo.ItemId);
            ViewBag.StoreId = new SelectList(db.Store, "Id", "Name", gQIInfo.StoreId);
            return View(gQIInfo);
        }

        // POST: GQIInfoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,ItemId,ReceivedQuantity,QCPassedQuantity,RejectedQuantity,FloorNo,RackNo,Cell,StoreId,GQIId")] GQIInfo gQIInfo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(gQIInfo).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.GQIId = new SelectList(db.GoodQualityInspection, "Id", "Id", gQIInfo.GQIId);
            ViewBag.ItemId = new SelectList(db.Item, "Id", "Name", gQIInfo.ItemId);
            ViewBag.StoreId = new SelectList(db.Store, "Id", "Name", gQIInfo.StoreId);
            return View(gQIInfo);
        }

        // GET: GQIInfoes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GQIInfo gQIInfo = db.GQIInfo.Find(id);
            if (gQIInfo == null)
            {
                return HttpNotFound();
            }
            return View(gQIInfo);
        }

        // POST: GQIInfoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            GQIInfo gQIInfo = db.GQIInfo.Find(id);
            db.GQIInfo.Remove(gQIInfo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
