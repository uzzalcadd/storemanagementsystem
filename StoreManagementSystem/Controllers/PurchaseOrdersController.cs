﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using StoreManagementSystem.Models;

namespace StoreManagementSystem.Controllers
{
    public class PurchaseOrdersController : Controller
    {
        private SMSDbContext db = new SMSDbContext();

        // GET: PurchaseOrders
        public ActionResult Index()
        {
            var purchaseOrder = db.PurchaseOrder.Include(p => p.Billto).Include(p => p.Delivery).Include(p => p.MKTPerson).Include(p => p.Requisition);
            return View(purchaseOrder.ToList());
        }

        // GET: PurchaseOrders/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PurchaseOrder purchaseOrder = db.PurchaseOrder.Find(id);
            if (purchaseOrder == null)
            {
                return HttpNotFound();
            }
            return View(purchaseOrder);
        }

        public ActionResult GetExistingItems()
        {
            string ItemName = Request["ItemName"];
            int ReqId = Convert.ToInt32(Request["reqId"]);
            List<PRMDetails> ItemList = new List<PRMDetails>();
            if (String.IsNullOrWhiteSpace(ItemName))
            {
                ItemList = db.PRMDetails.Where(i => i.PRMId == ReqId && i.Status == true).ToList();
            }
            else
            {
                ItemList = db.PRMDetails.Where(i => i.PRMId == ReqId && i.Status == true).Where(i => i.Item.Name.Contains(ItemName)).ToList();
            }
            return Json(ItemList, JsonRequestBehavior.AllowGet);
        }

        #region GetSelectedItems
        [AllowAnonymous]
        [HttpPost]
        public ActionResult GetSelectedItems()
        {
            if (Request["ItemId"].ToString() != "")
            {
                int ItemId = Convert.ToInt32(Request["ItemId"]);
                double Price = Convert.ToDouble(Request["Price"]);
                double Quantity = Convert.ToDouble(Request["Quantity"]);
                double Total = Convert.ToDouble(Request["Total"]);
                int SupplierId = Convert.ToInt32(Request["SupplierId"]);

                List<PurchaseOrderDetails> ItemList = new List<PurchaseOrderDetails>();
                ItemList = (List<PurchaseOrderDetails>)Session["ItemList"];
                if (ItemList.Count() > 0)
                {
                    PurchaseOrderDetails details = new PurchaseOrderDetails();
                    details.Id = ItemList.Count() + 1;
                    details.ItemId = ItemId;
                    details.Price = Price;
                    details.Quantity = Quantity;
                    details.TotalPrice = Total;
                    details.SuppierId = SupplierId;
                    details.Item = db.Item.Find(ItemId);
                    details.Supplier = db.Supplier.Find(SupplierId);
                    ItemList.Add(details);
                    Session["ItemList"] = ItemList;
                }
                else
                {
                    List<PurchaseOrderDetails> ItemLst = new List<PurchaseOrderDetails>();
                    PurchaseOrderDetails details = new PurchaseOrderDetails();
                    details.Id = 1;
                    details.ItemId = ItemId;
                    details.Price = Price;
                    details.Quantity = Quantity;
                    details.TotalPrice = Total;
                    details.SuppierId = SupplierId;
                    details.Item = db.Item.Find(ItemId);
                    details.Supplier = db.Supplier.Find(SupplierId);
                    ItemLst.Add(details);
                    Session["ItemList"] = ItemLst;
                }
            }
            return Json(Session["ItemList"], JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult DeleteFromList()
        {
            int DetailsId = Convert.ToInt32(Request["DetailsId"]);
            List<PurchaseOrderDetails> ItemLst = new List<PurchaseOrderDetails>();
            ItemLst = (List<PurchaseOrderDetails>)Session["ItemList"];
            for (int i = 0; i < ItemLst.Count; i++)
            {
                if (ItemLst[i].Id == DetailsId)
                {
                    ItemLst.Remove(ItemLst[i]);
                }
            }
            Session["ItemList"] = null;

            if (ItemLst.Count > 0)
            {
                Session["ItemList"] = ItemLst;
            }
            else
            {
                Session["ItemList"] = new List<PurchaseOrderDetails>();
            }

            return Json(Session["ItemList"], JsonRequestBehavior.AllowGet);
        }
        #endregion

        // GET: PurchaseOrders/Create
        public ActionResult Create()
        {
            if (db.PurchaseOrder.Count() > 0)
            {
                ViewBag.PurchaseOrderNo = db.PurchaseOrder.Max(i => i.PurchaseOrderNo) + 1;
            }
            else
            {
                ViewBag.PurchaseOrderNo = 1;
            }
            ViewBag.BillToId = new SelectList(db.BillTo, "Id", "Name");
            ViewBag.DeliveryId = new SelectList(db.Delivery, "Id", "Name");
            ViewBag.MKTPersonId = new SelectList(db.MKTPerson, "Id", "Name");
            ViewBag.ReqNoId = new SelectList(db.PRM.Where(i => i.Status == true), "Id", "ReqNo");
            ViewBag.SupplierId = new SelectList(db.Supplier, "Id", "Name");
            Session["ItemList"] = new List<PurchaseOrderDetails>();
            return View();
        }

        // POST: PurchaseOrders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,ReqNoId,PurchaseOrderNo,PurchaseOrderDate,SupplierId,DeliveryId,BillToId,MKTPersonId,Currency,CurrencyRate,PaymentTerm,Shipedby,TradeTerm,Note")] PurchaseOrder purchaseOrder)
        {
            List<PurchaseOrderDetails> ItemLst = new List<PurchaseOrderDetails>();
            ItemLst = (List<PurchaseOrderDetails>)Session["ItemList"];
            if (ItemLst.Count > 0)
            {
                purchaseOrder.PurchaseOrderDate = DateTime.Now.Date;
                purchaseOrder.Status = true;
                db.PurchaseOrder.Add(purchaseOrder);
                db.SaveChanges();

                #region Add Details
                List<PurchaseOrderDetails> ItemList = new List<PurchaseOrderDetails>();
                ItemList = (List<PurchaseOrderDetails>)Session["ItemList"];

                for (int i = 0; i < ItemList.Count; i++)
                {
                    PurchaseOrderDetails details = new PurchaseOrderDetails();
                    details.ItemId = ItemList[i].ItemId;
                    details.Price = ItemList[i].Price;
                    details.Quantity = ItemList[i].Quantity;
                    details.TotalPrice = ItemList[i].TotalPrice;
                    details.SuppierId = ItemList[i].SuppierId;
                    details.PurchaseOrderId = purchaseOrder.Id;
                    db.PurchaseOrderDetails.Add(details);
                    db.SaveChanges();

                    int prmDetailsId = db.PRMDetails.Where(x => x.PRMId == purchaseOrder.ReqNoId && x.ItemId == details.ItemId).Select(x => x.Id).FirstOrDefault();
                    PRMDetails prmDetails = db.PRMDetails.Find(prmDetailsId);
                    prmDetails.Status = false;
                    db.Entry(prmDetails).State = EntityState.Modified;
                    db.SaveChanges();
                }

                if(db.PRMDetails.Where(i=> i.PRMId == purchaseOrder.ReqNoId && i.Status == true).ToList().Count == 0)
                {
                    PRM prm = db.PRM.Find(purchaseOrder.ReqNoId);
                    prm.Status = false;
                    db.Entry(prm).State = EntityState.Modified;
                    db.SaveChanges();
                }
                #endregion

                return RedirectToAction("Index");
            }

            ViewBag.BillToId = new SelectList(db.BillTo, "Id", "Name", purchaseOrder.BillToId);
            ViewBag.DeliveryId = new SelectList(db.Delivery, "Id", "Name", purchaseOrder.DeliveryId);
            ViewBag.MKTPersonId = new SelectList(db.MKTPerson, "Id", "Name", purchaseOrder.MKTPersonId);
            ViewBag.ReqNoId = new SelectList(db.PRM.Where(i=> i.Status == true), "Id", "Name", purchaseOrder.ReqNoId);
            return View(purchaseOrder);
        }

        // GET: PurchaseOrders/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PurchaseOrder purchaseOrder = db.PurchaseOrder.Find(id);
            if (purchaseOrder == null)
            {
                return HttpNotFound();
            }
            ViewBag.BillToId = new SelectList(db.BillTo, "Id", "Name", purchaseOrder.BillToId);
            ViewBag.DeliveryId = new SelectList(db.Delivery, "Id", "Name", purchaseOrder.DeliveryId);
            ViewBag.MKTPersonId = new SelectList(db.MKTPerson, "Id", "Name", purchaseOrder.MKTPersonId);
            ViewBag.ReqNoId = new SelectList(db.PRM.Where(i => i.Status == true), "Id", "Note", purchaseOrder.ReqNoId);
            return View(purchaseOrder);
        }

        // POST: PurchaseOrders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,ReqNoId,PurchaseOrderNo,PurchaseOrderDate,SupplierId,DeliveryId,BillToId,MKTPersonId,Currency,CurrencyRate,PaymentTerm,Shipedby,TradeTerm,Note")] PurchaseOrder purchaseOrder)
        {
            if (ModelState.IsValid)
            {
                db.Entry(purchaseOrder).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.BillToId = new SelectList(db.BillTo, "Id", "Name", purchaseOrder.BillToId);
            ViewBag.DeliveryId = new SelectList(db.Delivery, "Id", "Name", purchaseOrder.DeliveryId);
            ViewBag.MKTPersonId = new SelectList(db.MKTPerson, "Id", "Name", purchaseOrder.MKTPersonId);
            ViewBag.ReqNoId = new SelectList(db.PRM.Where(i => i.Status == true), "Id", "Note", purchaseOrder.ReqNoId);
            return View(purchaseOrder);
        }

        #region Print
        public ActionResult PurchaseOrderByDateRange()
        {
            List<PurchaseOrder> PurchaseOrderList = new List<PurchaseOrder>();
            return View(PurchaseOrderList);
        }
        public ActionResult PurchaseOrderByDateRangeView()
        {
            DateTime FromDate = Convert.ToDateTime(Request["FromDate"]);
            DateTime ToDate = Convert.ToDateTime(Request["ToDate"]);
            List<PurchaseOrder> PurchaseOrderList = db.PurchaseOrder.Where(t => t.PurchaseOrderDate >= FromDate && t.PurchaseOrderDate <= ToDate).ToList();
            List<PurchaseOrderDetails> PurchaseOrderDetailsList = new List<PurchaseOrderDetails>();
            if (FromDate != null && PurchaseOrderList.Count() > 0)
            {
                foreach (var item in PurchaseOrderList)
                {
                    List<PurchaseOrderDetails> detailsList = db.PurchaseOrderDetails.Where(i => i.PurchaseOrderId == item.Id).ToList();
                    foreach (var itm in detailsList)
                    {
                        PurchaseOrderDetails details = db.PurchaseOrderDetails.Find(itm.Id);
                        PurchaseOrderDetailsList.Add(details);
                    }
                }
            }
            Session["PurchaseOrderList"] = PurchaseOrderList;
            Session["PurchaseOrderDetailsList"] = PurchaseOrderDetailsList;
            return View("PurchaseOrderByDateRange", PurchaseOrderList);
        }

        public ActionResult PrintPurchaseOrderByDateRange()
        {
            List<PurchaseOrder> PurchaseOrderList = new List<PurchaseOrder>();
            PurchaseOrderList = (List<PurchaseOrder>)Session["PurchaseOrderList"];
            Session["PurchaseOrderList"] = null;
            return View(PurchaseOrderList);
        }
        #endregion

        // GET: PurchaseOrders/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PurchaseOrder purchaseOrder = db.PurchaseOrder.Find(id);
            if (purchaseOrder == null)
            {
                return HttpNotFound();
            }
            return View(purchaseOrder);
        }

        // POST: PurchaseOrders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PurchaseOrder purchaseOrder = db.PurchaseOrder.Find(id);
            db.PurchaseOrder.Remove(purchaseOrder);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
