﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using StoreManagementSystem.Models;

namespace StoreManagementSystem.Controllers
{
    public class PurchaseOrderDetailsController : Controller
    {
        private SMSDbContext db = new SMSDbContext();

        // GET: PurchaseOrderDetails
        public ActionResult Index( int id)
        {
            var purchaseOrderDetails = db.PurchaseOrderDetails.Include(p => p.Item).Include(p => p.PurchaseOrder).Include(p => p.Supplier).Where(i => i.PurchaseOrderId  == id);
            return View(purchaseOrderDetails.ToList());
        }

        // GET: PurchaseOrderDetails/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PurchaseOrderDetails purchaseOrderDetails = db.PurchaseOrderDetails.Find(id);
            if (purchaseOrderDetails == null)
            {
                return HttpNotFound();
            }
            return View(purchaseOrderDetails);
        }

        // GET: PurchaseOrderDetails/Create
        public ActionResult Create()
        {
            ViewBag.ItemId = new SelectList(db.Item, "Id", "Name");
            ViewBag.PurchaseOrderId = new SelectList(db.PurchaseOrder, "Id", "Id");
            ViewBag.SuppierId = new SelectList(db.Supplier, "Id", "Name");
            return View();
        }

        // POST: PurchaseOrderDetails/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,ItemId,Quantity,Price,SuppierId,TotalPrice,PurchaseOrderId,Status")] PurchaseOrderDetails purchaseOrderDetails)
        {
            purchaseOrderDetails.Status = true;
            if (ModelState.IsValid)
            {
                db.PurchaseOrderDetails.Add(purchaseOrderDetails);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ItemId = new SelectList(db.Item, "Id", "Name", purchaseOrderDetails.ItemId);
            ViewBag.PurchaseOrderId = new SelectList(db.PurchaseOrder, "Id", "Id", purchaseOrderDetails.PurchaseOrderId);
            ViewBag.SuppierId = new SelectList(db.Supplier, "Id", "Name", purchaseOrderDetails.SuppierId);
            return View(purchaseOrderDetails);
        }

        // GET: PurchaseOrderDetails/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PurchaseOrderDetails purchaseOrderDetails = db.PurchaseOrderDetails.Find(id);
            if (purchaseOrderDetails == null)
            {
                return HttpNotFound();
            }
            ViewBag.ItemId = new SelectList(db.Item, "Id", "Name", purchaseOrderDetails.ItemId);
            ViewBag.PurchaseOrderId = new SelectList(db.PurchaseOrder, "Id", "Id", purchaseOrderDetails.PurchaseOrderId);
            ViewBag.SuppierId = new SelectList(db.Supplier, "Id", "Name", purchaseOrderDetails.SuppierId);
            return View(purchaseOrderDetails);
        }

        // POST: PurchaseOrderDetails/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,ItemId,Quantity,Price,SuppierId,TotalPrice,PurchaseOrderId")] PurchaseOrderDetails purchaseOrderDetails)
        {
            if (ModelState.IsValid)
            {
                db.Entry(purchaseOrderDetails).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ItemId = new SelectList(db.Item, "Id", "Name", purchaseOrderDetails.ItemId);
            ViewBag.PurchaseOrderId = new SelectList(db.PurchaseOrder, "Id", "Id", purchaseOrderDetails.PurchaseOrderId);
            ViewBag.SuppierId = new SelectList(db.Supplier, "Id", "Name", purchaseOrderDetails.SuppierId);
            return View(purchaseOrderDetails);
        }

        // GET: PurchaseOrderDetails/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PurchaseOrderDetails purchaseOrderDetails = db.PurchaseOrderDetails.Find(id);
            if (purchaseOrderDetails == null)
            {
                return HttpNotFound();
            }
            return View(purchaseOrderDetails);
        }

        // POST: PurchaseOrderDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PurchaseOrderDetails purchaseOrderDetails = db.PurchaseOrderDetails.Find(id);
            db.PurchaseOrderDetails.Remove(purchaseOrderDetails);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
