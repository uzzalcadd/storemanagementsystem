﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using StoreManagementSystem.Models;

namespace StoreManagementSystem.Controllers
{
    public class GRNmanagementsController : Controller
    {
        private SMSDbContext db = new SMSDbContext();

        #region List
        // GET: GRNmanagements
        public ActionResult Index()
        {
            var gRNmanagement = db.GRNmanagement.Include(g => g.PurchaseOrder).Include(g => g.Supplier);
            return View(gRNmanagement.ToList());
        }
        #endregion

        #region Details
        // GET: GRNmanagements/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GRNmanagement gRNmanagement = db.GRNmanagement.Find(id);
            if (gRNmanagement == null)
            {
                return HttpNotFound();
            }
            return View(gRNmanagement);
        }
        #endregion

        #region GetSelectedItems
        public ActionResult GetExistingOrders()
        {
            string ItemName = Request["ItemName"];
            int OrderId = Convert.ToInt32(Request["OrderId"]);

            List<PurchaseOrderDetails> OrderList = new List<PurchaseOrderDetails>();
            if (String.IsNullOrWhiteSpace(ItemName))
            {
                OrderList = db.PurchaseOrderDetails.Where(i => i.PurchaseOrderId == OrderId).ToList();
            }
            else
            {
                OrderList = db.PurchaseOrderDetails.Where(i => i.PurchaseOrderId == OrderId && i.Item.Name.Contains(ItemName)).ToList();
            }
            return Json(OrderList, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult GetSelectedItems()
        {
            if (Request["ItemId"].ToString() != "")
            {
                int ItemId = Convert.ToInt32(Request["ItemId"]);
                double Quantity = Convert.ToDouble(Request["Quantity"]);
                double RcvQuantity = Convert.ToDouble(Request["RcvQuantity"]);
                double YtrQuantity = Convert.ToDouble(Request["YtrQuantity"]);
                int LotNumber = Convert.ToInt32(Request["LotNumber"]);
                int SupplierId = Convert.ToInt32(Request["SupplierId"]);

                List<GRNDetails> ItemList = new List<GRNDetails>();
                ItemList = (List<GRNDetails>)Session["ItemList"];
                if (ItemList.Count() > 0)
                {
                    GRNDetails details = new GRNDetails();
                    details.Id = ItemList.Count() + 1;
                    details.ItemId = ItemId;
                    details.OrderedQuantity = Quantity;
                    details.ReceivedQuantity = RcvQuantity;
                    details.YetToReceive = YtrQuantity;
                    details.LotNumber = LotNumber;
                    details.SupplierId = SupplierId;
                    details.Item = db.Item.Find(ItemId);
                    details.Supplier = db.Supplier.Find(SupplierId);
                    ItemList.Add(details);
                    Session["ItemList"] = ItemList;
                }
                else
                {
                    List<GRNDetails> ItemLst = new List<GRNDetails>();
                    GRNDetails details = new GRNDetails();
                    details.Id = 1;
                    details.ItemId = ItemId;
                    details.OrderedQuantity = Quantity;
                    details.ReceivedQuantity = RcvQuantity;
                    details.YetToReceive = YtrQuantity;
                    details.LotNumber = LotNumber;
                    details.SupplierId = SupplierId;
                    details.Supplier = db.Supplier.Find(SupplierId);
                    details.Item = db.Item.Find(ItemId);
                    ItemLst.Add(details);
                    Session["ItemList"] = ItemLst;
                }
            }
            return Json(Session["ItemList"], JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult DeleteFromList()
        {
            int DetailsId = Convert.ToInt32(Request["DetailsId"]);
            List<GRNDetails> ItemLst = new List<GRNDetails>();
            ItemLst = (List<GRNDetails>)Session["ItemList"];
            for (int i = 0; i < ItemLst.Count; i++)
            {
                if (ItemLst[i].Id == DetailsId)
                {
                    ItemLst.Remove(ItemLst[i]);
                }
            }
            Session["ItemList"] = null;

            if (ItemLst.Count > 0)
            {
                Session["ItemList"] = ItemLst;
            }
            else
            {
                Session["ItemList"] = new List<GRNDetails>();
            }

            return Json(Session["ItemList"], JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Create
        // GET: GRNmanagements/Create
        public ActionResult Create()
        {
            if(db.GRNmanagement.Count() > 0)
            {
                ViewBag.GRNNumber = db.GRNmanagement.Max(i => i.GRNNumber) + 1;
            }
            else
            {
                ViewBag.GRNNumber = 1;
            }
            string grnType = "Purchase Order";
            ViewBag.GRNType = grnType;
            ViewBag.PurchaseOrderId = new SelectList(db.PurchaseOrder.Where(i => i.Status == true), "Id", "PurchaseOrderNo");
            Session["ItemList"] = new List<GRNDetails>();
            return View();
        }

        // POST: GRNmanagements/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,GRNType,GRNNumber,GRNDate,GLDate,PurchaseOrderId,Status,Remarks,ChallanDate,ChallanNo,GatePassNo,VehicleNo")] GRNmanagement gRNmanagement)
        {
            List<GRNDetails> ItemLst = new List<GRNDetails>();
            ItemLst = (List<GRNDetails>)Session["ItemList"];
            if (ItemLst.Count > 0)
            {
                PurchaseOrder pOrder = new PurchaseOrder();
                pOrder = db.PurchaseOrder.Where(i => i.Id == gRNmanagement.PurchaseOrderId).FirstOrDefault();
                pOrder.Status = false;
                gRNmanagement.GRNDate = DateTime.Now.Date;
                db.GRNmanagement.Add(gRNmanagement);
                db.SaveChanges();

                #region Add Details
                List<GRNDetails> ItemList = new List<GRNDetails>();
                ItemList = (List<GRNDetails>)Session["ItemList"];

                for (int i = 0; i < ItemList.Count; i++)
                {
                    GRNDetails details = new GRNDetails();
                    details.ItemId = ItemList[i].ItemId;
                    details.OrderedQuantity = ItemList[i].OrderedQuantity;
                    details.ReceivedQuantity = ItemList[i].ReceivedQuantity;
                    details.YetToReceive = ItemList[i].YetToReceive;
                    details.LotNumber = ItemList[i].LotNumber;
                    details.SupplierId = ItemList[i].SupplierId;
                    details.GRNId = gRNmanagement.Id;
                    db.GRNDetails.Add(details);
                    db.SaveChanges();
                }
                #endregion

                return RedirectToAction("Index");
            }

            ViewBag.PurchaseOrderId = new SelectList(db.PurchaseOrder.Where(i => i.Status == true), "Id", "PurchaseOrderNo", gRNmanagement.PurchaseOrderId);
            return View(gRNmanagement);
        }
        #endregion

        #region Edit
        // GET: GRNmanagements/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GRNmanagement gRNmanagement = db.GRNmanagement.Find(id);
            if (gRNmanagement == null)
            {
                return HttpNotFound();
            }
            ViewBag.PurchaseOrderId = new SelectList(db.PurchaseOrder.Where(i => i.Status == true), "Id", "Id", gRNmanagement.PurchaseOrderId);
            return View(gRNmanagement);
        }

        // POST: GRNmanagements/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,GRNType,GRNNumber,GRNDate,GLDate,PurchaseOrderId,Status,Remarks,ChallanDate,ChallanNo,GatePassNo,VehicleNo")] GRNmanagement gRNmanagement)
        {
            if (ModelState.IsValid)
            {
                db.Entry(gRNmanagement).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PurchaseOrderId = new SelectList(db.PurchaseOrder.Where(i => i.Status == true), "Id", "Id", gRNmanagement.PurchaseOrderId);
            return View(gRNmanagement);
        }
        #endregion

        #region Delete
        // GET: GRNmanagements/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GRNmanagement gRNmanagement = db.GRNmanagement.Find(id);
            if (gRNmanagement == null)
            {
                return HttpNotFound();
            }
            return View(gRNmanagement);
        }

        // POST: GRNmanagements/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            GRNmanagement gRNmanagement = db.GRNmanagement.Find(id);
            db.GRNmanagement.Remove(gRNmanagement);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        #endregion

        #region Dispose
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        #endregion
    }
}
