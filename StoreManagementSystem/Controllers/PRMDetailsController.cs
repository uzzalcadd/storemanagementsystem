﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using StoreManagementSystem.Models;

namespace StoreManagementSystem.Controllers
{
    public class PRMDetailsController : Controller
    {
        private SMSDbContext db = new SMSDbContext();

        // GET: PRMDetails
        public ActionResult Index( int id )
        {
            var pRMDetails = db.PRMDetails.Include(p => p.Item).Include(p => p.PRM).Where(i => i.PRMId == id);
            ViewBag.PRMId = id;
            return View(pRMDetails.ToList());
        }

        // GET: PRMDetails/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PRMDetails pRMDetails = db.PRMDetails.Find(id);
            if (pRMDetails == null)
            {
                return HttpNotFound();
            }
            return View(pRMDetails);
        }

        // GET: PRMDetails/Create
        public ActionResult Create( int id )
        {
            ViewBag.PRMId = id;
            ViewBag.ItemId = new SelectList(db.Item, "Id", "Name");
            ViewBag.PRMId = new SelectList(db.PRM, "Id", "Note");
            return View();
        }

        // POST: PRMDetails/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,ItemId,Quantity,RequiredFor,Remarks,PRMId")] PRMDetails pRMDetails)
        {
            if (ModelState.IsValid)
            {
                db.PRMDetails.Add(pRMDetails);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ItemId = new SelectList(db.Item, "Id", "Name", pRMDetails.ItemId);
            ViewBag.PRMId = new SelectList(db.PRM, "Id", "Note", pRMDetails.PRMId);
            return View(pRMDetails);
        }

        // GET: PRMDetails/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PRMDetails pRMDetails = db.PRMDetails.Find(id);
            if (pRMDetails == null)
            {
                return HttpNotFound();
            }
            ViewBag.ItemId = new SelectList(db.Item, "Id", "Name", pRMDetails.ItemId);
            ViewBag.PRMId = new SelectList(db.PRM, "Id", "Note", pRMDetails.PRMId);
            return View(pRMDetails);
        }

        // POST: PRMDetails/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,ItemId,Quantity,RequiredFor,Remarks,PRMId")] PRMDetails pRMDetails)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pRMDetails).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", new { id = pRMDetails.PRMId});
            }
            ViewBag.ItemId = new SelectList(db.Item, "Id", "Name", pRMDetails.ItemId);
            ViewBag.PRMId = new SelectList(db.PRM, "Id", "Note", pRMDetails.PRMId);
            return View(pRMDetails);
        }

        // GET: PRMDetails/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PRMDetails pRMDetails = db.PRMDetails.Find(id);
            if (pRMDetails == null)
            {
                return HttpNotFound();
            }
            return View(pRMDetails);
        }

        // POST: PRMDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PRMDetails pRMDetails = db.PRMDetails.Find(id);
            db.PRMDetails.Remove(pRMDetails);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
