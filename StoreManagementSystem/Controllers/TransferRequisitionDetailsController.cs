﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using StoreManagementSystem.Models;

namespace StoreManagementSystem.Controllers
{
    public class TransferRequisitionDetailsController : Controller
    {
        private SMSDbContext db = new SMSDbContext();

        // GET: TransferRequisitionDetails
        public ActionResult Index( int id)
        {
            var transferRequisitionDetails = db.TransferRequisitionDetails.Include(t => t.Item).Include(t => t.TransferRequisition).Where(i => i.TransferRequisitionId == id);
            ViewBag.TransferRequisitionId = id;
            return View(transferRequisitionDetails.ToList());
        }

        // GET: TransferRequisitionDetails/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TransferRequisitionDetails transferRequisitionDetails = db.TransferRequisitionDetails.Find(id);
            if (transferRequisitionDetails == null)
            {
                return HttpNotFound();
            }
            return View(transferRequisitionDetails);
        }

        // GET: TransferRequisitionDetails/Create
        public ActionResult Create(int id)
        {
            ViewBag.TransferRequisitionId = id;
            ViewBag.ItemId = new SelectList(db.Item, "Id", "Name");
            ViewBag.TransferRequisitionId = new SelectList(db.TransferRequisitionsIssue, "Id", "RequsitionType");
            return View();
        }

        // POST: TransferRequisitionDetails/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,ItemId,IssueQuantity,LotNumber,TransferRequisitionId")] TransferRequisitionDetails transferRequisitionDetails)
        {
            if (ModelState.IsValid)
            {
                db.TransferRequisitionDetails.Add(transferRequisitionDetails);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ItemId = new SelectList(db.Item, "Id", "Name", transferRequisitionDetails.ItemId);
            ViewBag.TransferRequisitionId = new SelectList(db.TransferRequisitionsIssue, "Id", "RequsitionType", transferRequisitionDetails.TransferRequisitionId);
            return View(transferRequisitionDetails);
        }

        // GET: TransferRequisitionDetails/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TransferRequisitionDetails transferRequisitionDetails = db.TransferRequisitionDetails.Find(id);
            if (transferRequisitionDetails == null)
            {
                return HttpNotFound();
            }
            ViewBag.ItemId = new SelectList(db.Item, "Id", "Name", transferRequisitionDetails.ItemId);
            ViewBag.TransferRequisitionId = new SelectList(db.TransferRequisitionsIssue, "Id", "RequsitionType", transferRequisitionDetails.TransferRequisitionId);
            return View(transferRequisitionDetails);
        }

        // POST: TransferRequisitionDetails/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,ItemId,IssueQuantity,LotNumber,TransferRequisitionId")] TransferRequisitionDetails transferRequisitionDetails)
        {
            if (ModelState.IsValid)
            {
                db.Entry(transferRequisitionDetails).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", new { id = transferRequisitionDetails.TransferRequisitionId});
            }
            ViewBag.ItemId = new SelectList(db.Item, "Id", "Name", transferRequisitionDetails.ItemId);
            ViewBag.TransferRequisitionId = new SelectList(db.TransferRequisitionsIssue, "Id", "RequsitionType", transferRequisitionDetails.TransferRequisitionId);
            return View(transferRequisitionDetails);
        }

        // GET: TransferRequisitionDetails/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TransferRequisitionDetails transferRequisitionDetails = db.TransferRequisitionDetails.Find(id);
            if (transferRequisitionDetails == null)
            {
                return HttpNotFound();
            }
            return View(transferRequisitionDetails);
        }

        // POST: TransferRequisitionDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TransferRequisitionDetails transferRequisitionDetails = db.TransferRequisitionDetails.Find(id);
            db.TransferRequisitionDetails.Remove(transferRequisitionDetails);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
