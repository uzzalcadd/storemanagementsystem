﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using StoreManagementSystem.Models;

namespace StoreManagementSystem.Controllers
{
    public class GRNDetailsController : Controller
    {
        private SMSDbContext db = new SMSDbContext();

        // GET: GRNDetails
        public ActionResult Index(int id)
        {
            var gRNDetails = db.GRNDetails.Include(g => g.GRNmanagement).Include(g => g.Item).Include(g => g.Supplier).Where(i => i.GRNId == id);
            ViewBag.GRNId = id;
            return View(gRNDetails.ToList());
        }

        // GET: GRNDetails/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GRNDetails gRNDetails = db.GRNDetails.Find(id);
            if (gRNDetails == null)
            {
                return HttpNotFound();
            }
            return View(gRNDetails);
        }

        // GET: GRNDetails/Create
        public ActionResult Create( int id)
        {
            ViewBag.GRNId = id;
            ViewBag.ItemId = new SelectList(db.Item, "Id", "Name");
            return View();
        }

        // POST: GRNDetails/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,ItemId,OrderedQuantity,ReceivedQuantity,YetToReceive,LotNumber,SupplierId,GRNId")] GRNDetails gRNDetails)
        {
            if (ModelState.IsValid)
            {
                db.GRNDetails.Add(gRNDetails);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.GRNId = new SelectList(db.GRNmanagement, "Id", "GRNType", gRNDetails.GRNId);
            ViewBag.ItemId = new SelectList(db.Item, "Id", "Name", gRNDetails.ItemId);
            return View(gRNDetails);
        }

        // GET: GRNDetails/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GRNDetails gRNDetails = db.GRNDetails.Find(id);
            if (gRNDetails == null)
            {
                return HttpNotFound();
            }
            ViewBag.GRNId = new SelectList(db.GRNmanagement, "Id", "GRNType", gRNDetails.GRNId);
            ViewBag.ItemId = new SelectList(db.Item, "Id", "Name", gRNDetails.ItemId);
            return View(gRNDetails);
        }

        // POST: GRNDetails/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,ItemId,OrderedQuantity,ReceivedQuantity,YetToReceive,LotNumber,GRNId")] GRNDetails gRNDetails)
        {
            if (ModelState.IsValid)
            {
                db.Entry(gRNDetails).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.GRNId = new SelectList(db.GRNmanagement, "Id", "GRNType", gRNDetails.GRNId);
            ViewBag.ItemId = new SelectList(db.Item, "Id", "Name", gRNDetails.ItemId);
            return View(gRNDetails);
        }

        // GET: GRNDetails/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GRNDetails gRNDetails = db.GRNDetails.Find(id);
            if (gRNDetails == null)
            {
                return HttpNotFound();
            }
            return View(gRNDetails);
        }

        // POST: GRNDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            GRNDetails gRNDetails = db.GRNDetails.Find(id);
            db.GRNDetails.Remove(gRNDetails);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
