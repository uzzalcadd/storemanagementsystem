﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using StoreManagementSystem.Models;

namespace StoreManagementSystem.Controllers
{
    public class PRMsController : Controller
    {
        private SMSDbContext db = new SMSDbContext();

        #region List
        // GET: PRMs
        public ActionResult Index()
        {
            var pRM = db.PRM.Include(p => p.Department).Include(p => p.ReqType);
            return View(pRM.ToList());
        }
        #endregion

        #region Details
        // GET: PRMs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PRM pRM = db.PRM.Find(id);
            if (pRM == null)
            {
                return HttpNotFound();
            }
            return View(pRM);
        }
        #endregion

        #region GetSelectedDetails
        [AllowAnonymous]
        [HttpPost]
        public ActionResult GetSelectedDetails()
        {
            if (Request["ItemId"].ToString() != "")
            {
                int ItemId = Convert.ToInt32(Request["ItemId"]);
                double Quantity = Convert.ToInt32(Request["Quantity"]);
                string RequiredFor = Request["RequiredFor"];
                string Remarks = Request["Remarks"];

                List<PRMDetails> PRMList = new List<PRMDetails>();
                PRMList = (List<PRMDetails>)Session["PRMList"];
                if (PRMList.Count() > 0)
                {
                    PRMDetails details = new PRMDetails();
                    details.Id = PRMList.Count() + 1;
                    details.ItemId = ItemId;
                    details.Quantity = Quantity;
                    details.RequiredFor = RequiredFor;
                    details.Remarks = Remarks;
                    details.Item = db.Item.Find(ItemId);
                    PRMList.Add(details);
                    Session["PRMList"] = PRMList;
                }
                else
                {
                    List<PRMDetails> PRMLst = new List<PRMDetails>();
                    PRMDetails details = new PRMDetails();
                    details.Id = 1;
                    details.ItemId = ItemId;
                    details.Quantity = Quantity;
                    details.RequiredFor = RequiredFor;
                    details.Remarks = Remarks;
                    details.Item = db.Item.Find(ItemId);
                    PRMLst.Add(details);
                    Session["PRMList"] = PRMLst;
                }
            }
            return Json(Session["PRMList"], JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult DeleteFromList()
        {
            int DetailsId = Convert.ToInt32(Request["DetailsId"]);
            List<PRMDetails> PRMLst = new List<PRMDetails>();
            PRMLst = (List<PRMDetails>)Session["PRMList"];
            for (int i = 0; i < PRMLst.Count; i++)
            {
                if (PRMLst[i].Id == DetailsId)
                {
                    PRMLst.Remove(PRMLst[i]);
                }
            }
            Session["PRMList"] = null;

            if (PRMLst.Count > 0)
            {
                Session["PRMList"] = PRMLst;
            }
            else
            {
                Session["PRMList"] = new List<PRMDetails>();
            }

            return Json(Session["PRMList"], JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Create
        // GET: PRMs/Create
        public ActionResult Create()
        {
            ViewBag.DepartmentId = new SelectList(db.Department, "Id", "Name");
            ViewBag.ReqTypeId = new SelectList(db.Reqtype, "Id", "ReqName");
            Session["PRMList"] = new List<PRMDetails>();
            return View();
        }

        // POST: PRMs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,ReqNo,ReqTypeId,ReqDate,RequrimentDate,DepartmentId,Note,Status")] PRM pRM)
        {
            List<PRMDetails> PRMLst = new List<PRMDetails>();
            PRMLst = (List<PRMDetails>)Session["PRMList"];
            if (PRMLst.Count > 0)
            {
                pRM.ReqDate = DateTime.Now.Date;
                pRM.Status = true;
                db.PRM.Add(pRM);
                db.SaveChanges();

                #region Add Details
                List<PRMDetails> PRMList = new List<PRMDetails>();
                PRMList = (List<PRMDetails>)Session["PRMList"];

                for (int i = 0; i < PRMList.Count; i++)
                {
                    PRMDetails details = new PRMDetails();
                    details.ItemId = PRMList[i].ItemId;
                    details.RequiredFor = PRMList[i].RequiredFor;
                    details.Quantity = PRMList[i].Quantity;
                    details.Remarks = PRMList[i].Remarks;
                    details.Status = true;
                    details.PRMId = pRM.Id;
                    db.PRMDetails.Add(details);
                    db.SaveChanges();
                }
                #endregion

                return RedirectToAction("Index");
            }

            ViewBag.DepartmentId = new SelectList(db.Department, "Id", "Name", pRM.DepartmentId);
            ViewBag.ReqTypeId = new SelectList(db.Reqtype, "Id", "ReqName", pRM.ReqTypeId);
            return View(pRM);
        }
        #endregion

        #region Edit
        // GET: PRMs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PRM pRM = db.PRM.Find(id);
            if (pRM == null)
            {
                return HttpNotFound();
            }
            ViewBag.DepartmentId = new SelectList(db.Department, "Id", "Sequence", pRM.DepartmentId);
            ViewBag.ReqTypeId = new SelectList(db.Reqtype, "Id", "ReqName", pRM.ReqTypeId);
            return View(pRM);
        }

        // POST: PRMs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,ReqNo,ReqTypeId,ReqDate,RequrimentDate,DepartmentId,ItemId,Note,Status")] PRM pRM)
        {
            if (ModelState.IsValid)
            {
                pRM.Status = true;
                db.Entry(pRM).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.DepartmentId = new SelectList(db.Department, "Id", "Name", pRM.DepartmentId);
            ViewBag.ReqTypeId = new SelectList(db.Reqtype, "Id", "ReqName", pRM.ReqTypeId);
            return View(pRM);
        }
        #endregion

        #region Delete
        // GET: PRMs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PRM pRM = db.PRM.Find(id);
            if (pRM == null)
            {
                return HttpNotFound();
            }
            return View(pRM);
        }

        // POST: PRMs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PRM pRM = db.PRM.Find(id);
            db.PRM.Remove(pRM);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        #endregion

        #region Dispose
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        #endregion
    }
}
