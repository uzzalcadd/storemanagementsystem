﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using StoreManagementSystem.Models;

namespace StoreManagementSystem.Controllers
{
    public class MKTPersonsController : Controller
    {
        private SMSDbContext db = new SMSDbContext();

        #region List
        // GET: MKTPersons
        public ActionResult Index()
        {
            return View(db.MKTPerson.ToList());
        }
        #endregion

        #region GetExistingMKTPerson
        public ActionResult GetExistingMKTPerson()
        {
            string MKTPersonName = Request["MKTPersonName"];
            List<MKTPerson> MKTpersonList = new List<MKTPerson>();
            if(String.IsNullOrWhiteSpace(MKTPersonName))
            {
                MKTpersonList = db.MKTPerson.ToList();
            }
            else
            {
                MKTpersonList = db.MKTPerson.Where(i => i.Name.Contains(MKTPersonName)).ToList();
            }
            return Json(MKTpersonList, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Details
        // GET: MKTPersons/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MKTPerson mKTPerson = db.MKTPerson.Find(id);
            if (mKTPerson == null)
            {
                return HttpNotFound();
            }
            return View(mKTPerson);
        }
        #endregion

        #region Create 
        // GET: MKTPersons/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: MKTPersons/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Code,Designation")] MKTPerson mKTPerson)
        {
            if (ModelState.IsValid)
            {
                db.MKTPerson.Add(mKTPerson);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(mKTPerson);
        }
        #endregion

        #region Edit
        // GET: MKTPersons/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MKTPerson mKTPerson = db.MKTPerson.Find(id);
            if (mKTPerson == null)
            {
                return HttpNotFound();
            }
            return View(mKTPerson);
        }

        // POST: MKTPersons/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Code,Designation")] MKTPerson mKTPerson)
        {
            if (ModelState.IsValid)
            {
                db.Entry(mKTPerson).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(mKTPerson);
        }
        #endregion

        #region Delete
        // GET: MKTPersons/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MKTPerson mKTPerson = db.MKTPerson.Find(id);
            if (mKTPerson == null)
            {
                return HttpNotFound();
            }
            return View(mKTPerson);
        }
       

        // POST: MKTPersons/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            MKTPerson mKTPerson = db.MKTPerson.Find(id);
            db.MKTPerson.Remove(mKTPerson);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        #endregion

        #region Dispose
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        #endregion
    }
}
